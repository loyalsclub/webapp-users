/** External dependencies */
import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

/** Components */
import {
  CustomInput,
  PersonalInfoSchema,
  EmailSchema,
} from '../Common/Form.component';
import translate from '../../utils/i18n.utils';

const CompleteProfile = ({
  completeUserSocialLogin,
  signOut,
  error,
  newUser,
  currentStep,
  findPendingUser,
}) => (
  <div className="auth-centered">
    <p className="error">{error && error.text}</p>
    <br />
    <br />

    {currentStep === 1 ? (
      <>
        <p>{translate('COMPLETE_PROFILE.intro')}</p>

        <Formik
          initialValues={{ email: newUser.email || '' }}
          validationSchema={EmailSchema}
          enableReinitialize
          onSubmit={({ email }, { setSubmitting }) => {
            findPendingUser({ email });
            setSubmitting(false);
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <div className="input-wrapper">
                <label htmlFor="email">
                  {translate('COMPLETE_PROFILE.email')}
                </label>

                <br />

                <Field type="email" name="email" component={CustomInput} />

                <ErrorMessage
                  name="email"
                  component="p"
                  className="error-field"
                />
              </div>

              <div className="input-wrapper submit">
                <input
                  type="submit"
                  className="rounded-button active"
                  value={translate('GENERIC.continue')}
                  disabled={isSubmitting}
                />
              </div>
            </Form>
          )}
        </Formik>
      </>
    ) : null}

    {currentStep === 2 ? (
      <Formik
        initialValues={{
          fname: newUser.firstName || '',
          lname: newUser.lastName || '',
          bday: '',
          bmonth: '',
          byears: '',
        }}
        enableReinitialize
        validationSchema={PersonalInfoSchema}
        onSubmit={async (values, { setSubmitting }) => {
          const date = new Date(values.byears, values.bmonth - 1, values.bday);

          completeUserSocialLogin({
            variables: {
              ...newUser,
              birthDate: date.toISOString(),
              lastName: values.lname,
              firstName: values.fname,
            },
          });

          setSubmitting(false);
        }}
      >
        {({ isSubmitting }) => (
          <>
            <Form>
              <div className="input-wrapper">
                <label htmlFor="firstName">
                  {translate('GENERIC.firstName')}
                </label>

                <br />

                <Field type="text" name="fname" component={CustomInput} />

                <ErrorMessage
                  name="fname"
                  component="p"
                  className="error-field"
                />
              </div>

              <div className="input-wrapper">
                <label htmlFor="lastName">
                  {translate('GENERIC.lastName')}
                </label>

                <br />

                <Field type="text" name="lname" component={CustomInput} />

                <ErrorMessage
                  name="lname"
                  component="p"
                  className="error-field"
                />
              </div>

              <div className="input-wrapper">
                <label htmlFor="bday">{translate('GENERIC.birthDate')}</label>

                <br />

                <div className="date-item-container">
                  <div className="date-item">
                    <span className="date-item-label">
                      {translate('GENERIC.day')}
                    </span>

                    <Field
                      className="date day"
                      type="number"
                      name="bday"
                      autoComplete="off"
                      component={CustomInput}
                    />
                  </div>

                  <div className="date-item">
                    <span className="date-item-label">
                      {translate('GENERIC.month')}
                    </span>

                    <Field
                      className="date month"
                      type="number"
                      name="bmonth"
                      autoComplete="off"
                      component={CustomInput}
                    />
                  </div>

                  <div className="date-item">
                    <span className="date-item-label">
                      {translate('GENERIC.year')}
                    </span>

                    <Field
                      className="date year"
                      type="number"
                      name="byears"
                      autoComplete="off"
                      component={CustomInput}
                    />
                  </div>

                  <ErrorMessage
                    name="bday"
                    component="p"
                    className="error-field"
                  />

                  <ErrorMessage
                    name="bmonth"
                    component="p"
                    className="error-field"
                  />

                  <ErrorMessage
                    name="byears"
                    component="p"
                    className="error-field"
                  />
                </div>
              </div>

              <div className="input-wrapper">
                <input
                  type="submit"
                  className="rounded-button active"
                  value={translate('GENERIC.continue')}
                  disabled={isSubmitting}
                />
              </div>
            </Form>
          </>
        )}
      </Formik>
    ) : null}

    <button type="button" className="secondary-button" onClick={signOut}>
      {translate('COMPLETE_PROFILE.logOut')}
    </button>
  </div>
);

export default CompleteProfile;
