/** External dependencies */
import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import { useLazyQuery, useMutation } from '@apollo/client';

/** Providers */
import { analyticsProvider, events } from '../../providers/analytics';

/** Components */
import {
  CustomInput,
  PersonalInfoSchema,
  EmailSchema,
  PasswordSchema,
} from '../Common/Form.component';

/** Queries */
import {
  FIND_PENDING_USER,
  CREATE_USER,
  COMPLETE_PENDING_USER,
} from '../../queries/user.queries';

/** Utils */
import { parseError } from '../../utils/errors.utils';
import translate from '../../utils/i18n.utils';

const Register = (props) => {
  const { loginAfterRegistration } = props;
  const [currentStep, setCurrentStep] = useState(1);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  /** Register */
  const [newUser, setNewUser] = useState({ firstName: '', lastName: '' });
  analyticsProvider.logEvent(events.registrationLoyals);

  const handleRequestError = (queryError) => {
    const err = parseError(queryError);
    if (!err) return;
    setError({
      text: err.text,
    });
    setLoading(false);
  };

  const [findPendingUser] = useLazyQuery(FIND_PENDING_USER, {
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      setNewUser({ ...newUser, ...data.findPendingUser });
      setLoading(false);
      setError(null);
      if (data.findPendingUser && !data.findPendingUser.isPending) {
        return setError({
          text: translate('ERRORS.emailAlreadyRegistered'),
        });
      }
      // If its pending we log the corresponding event
      if (data.findPendingUser && data.findPendingUser.isPending) {
        analyticsProvider.logEvent(events.registrationEmailStepPending);
      } else {
        analyticsProvider.logEvent(events.registrationEmailStep);
      }

      setCurrentStep(2);
    },
    onError: handleRequestError,
  });

  const [createUser] = useMutation(CREATE_USER, {
    options: () => ({
      fetchPolicy: 'no-cache',
    }),
    onCompleted: ({ createUser: { loginToken } }) => {
      analyticsProvider.logEvent(events.registrationCompleted);
      setLoading(false);
      loginAfterRegistration(loginToken);
    },
    onError: handleRequestError,
  });

  const [completePendingUser] = useMutation(COMPLETE_PENDING_USER, {
    options: () => ({
      fetchPolicy: 'no-cache',
    }),
    onCompleted: ({ completePendingUser: { loginToken } }) => {
      analyticsProvider.logEvent(events.registrationCompleted);
      setLoading(false);
      loginAfterRegistration(loginToken);
    },
    onError: handleRequestError,
  });

  const retrieveUserInfo = async (email) => {
    setNewUser({ ...newUser, email });
    findPendingUser({
      variables: {
        email,
      },
    });
  };

  return (
    <div className="auth-centered">
      <p className="error">{error && error.text}</p>
      <br />
      <br />

      {currentStep === 1 ? (
        <Formik
          initialValues={{ email: '' }}
          validationSchema={EmailSchema}
          onSubmit={({ email }, { setSubmitting }) => {
            setError(null);
            retrieveUserInfo(email);
            setSubmitting(false);
          }}
        >
          {({ isSubmitting }) => (
            <Form>
              <div className="input-wrapper">
                <label htmlFor="email">{translate('GENERIC.email')}</label>

                <br />

                <Field type="email" name="email" component={CustomInput} />

                <ErrorMessage
                  name="email"
                  component="p"
                  className="error-field"
                />
              </div>

              <div className="input-wrapper submit">
                <input
                  type="submit"
                  className="rounded-button active"
                  value={translate('GENERIC.continue')}
                  disabled={isSubmitting}
                />
              </div>
            </Form>
          )}
        </Formik>
      ) : null}

      {currentStep === 2 ? (
        <Formik
          initialValues={{
            fname: newUser.firstName || '',
            lname: newUser.lastName || '',
            bday: '',
            bmonth: '',
            byears: '',
          }}
          enableReinitialize
          validationSchema={PersonalInfoSchema}
          onSubmit={async (values, { setSubmitting }) => {
            // let schema = Yup.date();
            const date = new Date(
              values.byears,
              values.bmonth - 1,
              values.bday
            );
            // const validDate = await schema.isValid(date);

            analyticsProvider.logEvent(events.registrationUserDataStep);
            setNewUser({
              ...newUser,
              birthDate: date.toISOString(),
              lastName: values.lname,
              firstName: values.fname,
            });
            setSubmitting(false);
            setCurrentStep(3);
          }}
        >
          {() => (
            <>
              <Form>
                <div className="input-wrapper">
                  <label htmlFor="fname">
                    {translate('GENERIC.firstName')}
                  </label>

                  <br />

                  <Field type="text" name="fname" component={CustomInput} />

                  <ErrorMessage
                    name="fname"
                    component="p"
                    className="error-field"
                  />
                </div>

                <div className="input-wrapper">
                  <label htmlFor="lname">{translate('GENERIC.lastName')}</label>

                  <br />

                  <Field type="text" name="lname" component={CustomInput} />

                  <ErrorMessage
                    name="lname"
                    component="p"
                    className="error-field"
                  />
                </div>

                <div className="input-wrapper">
                  <label htmlFor="bday">{translate('GENERIC.birthDate')}</label>

                  <br />

                  <div className="date-item-container">
                    <div className="date-item">
                      <span className="date-item-label">
                        {translate('GENERIC.day')}
                      </span>

                      <Field
                        className="date day"
                        type="number"
                        name="bday"
                        autoComplete="off"
                        component={CustomInput}
                      />
                    </div>

                    <div className="date-item">
                      <span className="date-item-label">
                        {translate('GENERIC.month')}
                      </span>

                      <Field
                        className="date month"
                        type="number"
                        name="bmonth"
                        autoComplete="off"
                        component={CustomInput}
                      />
                    </div>

                    <div className="date-item">
                      <span className="date-item-label">
                        {translate('GENERIC.year')}
                      </span>

                      <Field
                        className="date year"
                        type="number"
                        name="byears"
                        autoComplete="off"
                        component={CustomInput}
                      />
                    </div>

                    <ErrorMessage
                      name="bday"
                      component="p"
                      className="error-field"
                    />

                    <ErrorMessage
                      name="bmonth"
                      component="p"
                      className="error-field"
                    />

                    <ErrorMessage
                      name="byears"
                      component="p"
                      className="error-field"
                    />
                  </div>
                </div>

                <div className="input-wrapper">
                  <input
                    type="submit"
                    className="rounded-button active"
                    value={translate('GENERIC.continue')}
                    disabled={loading}
                  />
                </div>
              </Form>
            </>
          )}
        </Formik>
      ) : null}

      {currentStep === 3 ? (
        <Formik
          initialValues={{ password: '', repeatpassword: '' }}
          validationSchema={PasswordSchema}
          onSubmit={({ password }, { setSubmitting }) => {
            setSubmitting(false);
            const req = { variables: { ...newUser, password } };
            if (newUser.isPending) return completePendingUser(req);
            return createUser(req);
          }}
        >
          {() => (
            <Form>
              <div className="input-wrapper">
                <label htmlFor="password">
                  {`${translate('GENERIC.password')} `}
                  <span className="label-note">
                    ({translate('REGISTER.atLeastChars')})
                  </span>
                </label>

                <br />

                <Field
                  type="password"
                  name="password"
                  component={CustomInput}
                />

                <ErrorMessage
                  name="password"
                  component="p"
                  className="error-field"
                />
              </div>

              <div className="input-wrapper">
                <label htmlFor="repeatpassword">
                  {translate('REGISTER.repeatPassword')}
                </label>

                <br />

                <Field
                  type="password"
                  name="repeatpassword"
                  component={CustomInput}
                />

                <ErrorMessage
                  name="repeatpassword"
                  component="p"
                  className="error-field"
                />
              </div>

              <div className="input-wrapper submit">
                <input
                  type="submit"
                  className="rounded-button active"
                  value={translate('GENERIC.continue')}
                  disabled={loading}
                />
              </div>
            </Form>
          )}
        </Formik>
      ) : null}
    </div>
  );
};

export default Register;
