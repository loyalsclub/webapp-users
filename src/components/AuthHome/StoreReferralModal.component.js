import React, { useState } from 'react';
import Modal from 'react-modal';
import StoreLogoComponent from '../Common/StoreLogo.component';
import translate from '../../utils/i18n.utils';

const StoreReferralModal = ({ storeData }) => {
  const [isOpen, setModalOpen] = useState(true);
  const closeModal = () => setModalOpen(false);

  if (!storeData) return null;

  return (
    <Modal
      isOpen={isOpen}
      overlayClassName="loyals-club-referral-user-tutorial-overlay"
      className="loyals-club-referral-user-tutorial-content"
      shouldCloseOnOverlayClick
      onRequestClose={closeModal}
      closeTimeoutMS={300}
    >
      <div className="referral-user-header">
        <button
          type="button"
          className="referral-user-close-icon"
          onClick={closeModal}
        >
          <i className="icon-cross" />
        </button>
      </div>

      <div className="referral-user-body">
        <div className="welcome-step">
          <StoreLogoComponent
            src={storeData.profileImage}
            style={{
              top: 0,
              width: 150,
              height: 150,
              margin: '0 auto',
              alt: 'Logo',
            }}
          />

          <h1>{translate('STORE_REFERRAL_MODAL.title')}</h1>

          <p
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{
              __html: translate('STORE_REFERRAL_MODAL.invited', {
                storeName: `<b>${storeData.name}</b>`,
              }),
            }}
          />

          <p>
            {translate('STORE_REFERRAL_MODAL.register')}
            <br />
          </p>

          <button
            type="button"
            className="rounded-button active"
            onClick={closeModal}
          >
            {translate('GENERIC.continue')}
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default StoreReferralModal;
