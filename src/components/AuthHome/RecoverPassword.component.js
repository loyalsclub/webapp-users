/** External dependencies */
import React, { useState } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';

/** Components */
import { CustomInput } from '../Common/Form.component';
import translate from '../../utils/i18n.utils';

const RecoverPassword = ({ recoverPassword, error }) => {
  const [currentStep, setCurrentStep] = useState(1);

  return (
    <div className="auth-centered">
      {currentStep === 1 ? (
        <>
          <p>{translate('RECOVER_PASSWORD.askForEmail')}</p>

          <p className="error">{error && error.text}</p>

          <br />
          <br />

          <Formik
            initialValues={{ email: '', password: '' }}
            onSubmit={async ({ email }, { setSubmitting }) => {
              await recoverPassword(email);
              setSubmitting(false);
              setCurrentStep(2);
            }}
          >
            {({ isSubmitting }) => (
              <Form noValidate>
                <div className="input-wrapper">
                  <label htmlFor="email">{translate('GENERIC.email')}</label>

                  <br />

                  <Field type="email" name="email" component={CustomInput} />

                  <ErrorMessage
                    name="email"
                    component="p"
                    className="error-field"
                  />
                </div>

                <div className="input-wrapper submit">
                  <input
                    type="submit"
                    className="rounded-button active"
                    value={translate('RECOVER_PASSWORD.recoverButton')}
                    disabled={isSubmitting}
                  />
                </div>
              </Form>
            )}
          </Formik>
        </>
      ) : null}

      {currentStep === 2 ? (
        <p>{translate('RECOVER_PASSWORD.emailSent')}</p>
      ) : null}
    </div>
  );
};

export default RecoverPassword;
