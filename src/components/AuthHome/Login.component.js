/** External dependencies */
import React from 'react';
import Link from 'next/link';
import { Formik, Form, Field, ErrorMessage } from 'formik';

/** Components */
import { CustomInput } from '../Common/Form.component';
import translate from '../../utils/i18n.utils';

const Login = ({ loginEmailPassword, error }) => (
  <div className="auth-centered">
    <p className="error">{error && error.text}</p>

    <br />

    <br />

    <Formik
      initialValues={{ email: '', password: '' }}
      onSubmit={async (values, { setSubmitting }) => {
        await loginEmailPassword(values, () => {
          setSubmitting(false);
        });
      }}
    >
      {({ isSubmitting }) => (
        <Form noValidate>
          <div className="input-wrapper">
            <label htmlFor="email">{translate('GENERIC.email')}</label>

            <br />

            <Field type="email" name="email" component={CustomInput} />

            <ErrorMessage name="email" component="p" className="error-field" />
          </div>

          <div className="input-wrapper">
            <label htmlFor="password">{translate('GENERIC.password')}</label>

            <br />

            <Field type="password" name="password" component={CustomInput} />

            <ErrorMessage
              name="password"
              component="p"
              className="error-field"
            />
          </div>

          <div className="input-wrapper submit">
            <input
              type="submit"
              className="rounded-button active"
              value={translate('GENERIC.login')}
              disabled={isSubmitting}
            />
          </div>
        </Form>
      )}
    </Formik>

    <Link className="forgot-password" href="recover-password">
      {translate('LOGIN.forgotPassword')}
    </Link>
  </div>
);

export default Login;
