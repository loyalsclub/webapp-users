/** External dependencies */
import React, { useState } from 'react';

/** Components */
import RewardListComponent from '../UserHome/StoresComponents/RewardList.component';
import UserHomePageContent from '../UserHome/UserHomePageContent.component';
import StoreInformationComponent from '../UserHome/StoresComponents/StoreInformation.component';
import StoreReferralModalComponent from './StoreReferralModal.component';
import StoreHeaderComponent from '../Common/StoreHeader.component';
import translate from '../../utils/i18n.utils';

const StoreInfo = (props) => {
  const [activeTab, onChangeTab] = useState(1);

  const getContent = () => {
    const { Store, storeLevelsAndRewardsById, onJoin } = props;

    return (
      <>
        <StoreReferralModalComponent storeData={Store} />

        <StoreHeaderComponent
          activeTab={activeTab}
          onChangeTab={onChangeTab}
          Store={Store}
        />

        <div className="store-content">
          <h2 className="storeinformation-store-name">{Store.name}</h2>

          <div className="customerprofile-content">
            {activeTab === 1 ? (
              <RewardListComponent
                customerProfileGetStoreLevelsWithRewards={
                  storeLevelsAndRewardsById
                }
              />
            ) : null}

            {activeTab === 2 ? (
              <StoreInformationComponent alias={Store.alias} />
            ) : null}
          </div>
        </div>

        <div className="sticky-join">
          <button
            type="button"
            className="rounded-button active"
            onClick={onJoin}
          >
            {translate('GENERIC.join')}
          </button>
        </div>
      </>
    );
  };

  return (
    <UserHomePageContent
      {...props}
      rootClassName="storeinformation-content"
      getContent={getContent}
    />
  );
};

export default StoreInfo;
