import React from 'react';
import SlickSlider from 'react-slick';
import Image from 'next/image';
import translate from '../../utils/i18n.utils';

const settings = {
  className: '',
  arrows: false,
  dots: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 4000,
};

const Slider = () => (
  <div className="slick-container">
    <div className="logo-mobile">
      <Image
        width={120}
        height={35}
        src="/img/logo-white.png"
        alt="Loyals Club"
        priority
      />
    </div>

    <SlickSlider {...settings}>
      <div className="slide slide1">
        <div className="slide-content">
          <h2>{translate('SLIDER.firstSlideDescription')}</h2>

          <div className="slide-contant-separator">
            <Image
              width={150}
              height={3}
              src="/img/auth-home/separador.png"
              alt={translate('SLIDER.firstSlideAlt')}
              priority
            />
          </div>
        </div>
      </div>

      <div className="slide slide2">
        <div className="slide-content">
          <h2>{translate('SLIDER.secondSlideDescription')}</h2>

          <div className="slide-contant-separator">
            <Image
              width={150}
              height={3}
              src="/img/auth-home/separador.png"
              alt={translate('SLIDER.secondSlideAlt')}
              priority
            />
          </div>
        </div>
      </div>

      <div className="slide slide3">
        <div className="slide-content">
          <h2>{translate('SLIDER.thirdSlideDescription')}</h2>

          <div className="slide-contant-separator">
            <Image
              width={150}
              height={3}
              src="/img/auth-home/separador.png"
              alt={translate('SLIDER.thirdSlideAlt')}
              priority
            />
          </div>
        </div>
      </div>
    </SlickSlider>

    <div className="border-slider-mobile">
      <Image
        width={767}
        height={100}
        layout="responsive"
        src="/img/auth-home/border-slider-mobile.png"
        alt="-"
        priority
      />
    </div>
  </div>
);

export default Slider;
