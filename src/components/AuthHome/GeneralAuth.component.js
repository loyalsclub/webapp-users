/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/control-has-associated-label */

import React from 'react';
import Image from 'next/image';
import StoreLogoComponent from '../Common/StoreLogo.component';
import translate from '../../utils/i18n.utils';

const GeneralAuth = ({ storeData, loginGoogle, loginFacebook, redirectTo }) => (
  <div className="auth-centered">
    {storeData ? (
      <StoreLogoComponent
        src={storeData.profileImage}
        style={{
          top: 0,
          width: 150,
          height: 150,
          margin: '10px auto 20px',
          alt: translate('GENERAL_AUTH.logoAlt'),
        }}
      />
    ) : (
      <div className="ticket-guy">
        <Image
          width={150}
          height={150}
          src="/img/auth-home/ticket-guy.png"
          alt={translate('GENERAL_AUTH.ticketGuyAlt')}
        />
      </div>
    )}

    <div className="login-methods">
      <label htmlFor="login">{translate('GENERAL_AUTH.loginWith')}</label>

      <br />

      <div className="social-login">
        <button
          type="button"
          className="social-media-button fb-login"
          onClick={loginFacebook}
        />

        <button
          type="button"
          className="social-media-button gg-login"
          onClick={loginGoogle}
        />
      </div>

      <button
        type="button"
        className="rounded-button active"
        onClick={redirectTo('/login')}
      >
        {translate('GENERIC.login')}
      </button>

      <button
        type="button"
        className="rounded-button dark"
        onClick={redirectTo('/register')}
      >
        {translate('GENERAL_AUTH.register')}
      </button>
    </div>
  </div>
);

export default GeneralAuth;
