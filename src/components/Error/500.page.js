/** External dependencies */
import React from 'react';

/** Components */
import GenericMessageComponent from '../Common/GenericMessage.component';

/** Utils */
import { SERVER_ERROR } from '../../utils/errors.utils';
import translate from '../../utils/i18n.utils';

const Page500 = () => (
  <div className="full-centered-container">
    <GenericMessageComponent
      message={{
        text: translate(SERVER_ERROR.text),
      }}
    />
  </div>
);

export default Page500;
