/** External dependencies */
import React, { useState } from 'react';
import { useSpring, animated } from 'react-spring';

/** Components */
import ProgressComponent from './StoresComponents/Progress.component';
import RewardListComponent from './StoresComponents/RewardList.component';
import UserHomePageContent from './UserHomePageContent.component';
import StoreInformationComponent from './StoresComponents/StoreInformation.component';
import StoreHeader from '../Common/StoreHeader.component';
import translate from '../../utils/i18n.utils';

const CustomerProfile = (props) => {
  const [activeTab, onChangeTab] = useState(1);
  const propsAnim = useSpring({ opacity: 1, from: { opacity: 0 } });

  const getContent = () => {
    const {
      customerProfileGetStoreLevelsWithRewards,
      userCustomerProfileByStoreId: {
        Store,
        CustomerLevel: {
          nextLevelPoints,
          nextLevelPointsDif,
          currentLevelPoints,
          level,
          total,
        },
      },
      pointsLoaded = 0,
      pointsError = false,
      onUserProfile,
    } = props;

    return (
      <>
        <StoreHeader
          onChangeTab={onChangeTab}
          activeTab={activeTab}
          Store={Store}
        />

        <div className="customerprofile-information">
          <h2 className="customerprofile-store-name">{Store.name}</h2>

          <div className="customerprofile-content">
            {activeTab === 1 ? (
              <>
                <ProgressComponent
                  size={270}
                  level={level}
                  total={total}
                  nextLevelPoints={nextLevelPoints}
                  currentLevelPoints={currentLevelPoints}
                  nextLevelPointsDif={nextLevelPointsDif}
                  profileImage={Store.profileImage}
                />

                {pointsLoaded > 0 ? (
                  <animated.div
                    style={propsAnim}
                    className="customerprofile-message success"
                  >
                    {translate('CUSTOMER_PROFILE.pointsLoaded', {
                      count: pointsLoaded,
                      pointsLoaded,
                    })}
                  </animated.div>
                ) : null}

                {pointsError ? (
                  <animated.div
                    style={propsAnim}
                    className="customerprofile-message fail"
                  >
                    {translate('CUSTOMER_PROFILE.pointsError')}
                  </animated.div>
                ) : null}

                <RewardListComponent
                  currentLevel={level}
                  customerProfileGetStoreLevelsWithRewards={
                    customerProfileGetStoreLevelsWithRewards
                  }
                />
              </>
            ) : null}

            {activeTab === 2 ? (
              <StoreInformationComponent alias={Store.alias} />
            ) : null}
          </div>
        </div>

        <button type="button" onClick={onUserProfile} className="float-button">
          <i className="icon-qr" />
        </button>
      </>
    );
  };

  return (
    <UserHomePageContent
      {...props}
      rootClassName="usercustomerprofile-content"
      getContent={getContent}
    />
  );
};

export default CustomerProfile;
