import React, { useState } from 'react';
import Modal from 'react-modal';
import translate from '../../utils/i18n.utils';

const WelcomeModal = ({ isOpen, closeModal }) => {
  const [step, setStep] = useState(0);

  const nextStep = () => {
    if (step < 3) {
      setStep(step + 1);
      return;
    }

    closeModal();
  };

  return (
    <Modal
      isOpen={isOpen}
      overlayClassName="loyals-club-new-user-tutorial-overlay"
      className="loyals-club-new-user-tutorial-content"
      shouldCloseOnOverlayClick
      onRequestClose={closeModal}
      closeTimeoutMS={300}
    >
      <div className="welcome-user-header">
        <button
          type="button"
          className="welcome-user-close-icon"
          onClick={closeModal}
        >
          <i className="icon-cross" />
        </button>
      </div>

      <div className="welcome-user-body">
        {step === 0 ? (
          <div className="welcome-step">
            <h1>{translate('WELCOME_MODAL.firstStepTitle')}</h1>

            <p>{translate('WELCOME_MODAL.firstStepParagraph1')}</p>

            <p>{translate('WELCOME_MODAL.firstStepParagraph2')}</p>

            <button
              type="button"
              className="rounded-button active"
              onClick={nextStep}
            >
              {translate('WELCOME_MODAL.firstStepAction1')}
            </button>

            <button type="button" className="text-button" onClick={closeModal}>
              {translate('WELCOME_MODAL.firstStepAction2')}
            </button>
          </div>
        ) : null}

        {step === 1 ? (
          <div className="welcome-step">
            <h1>{translate('WELCOME_MODAL.secondStepTitle')}</h1>

            <p>{translate('WELCOME_MODAL.secondStepParagraph1')}</p>

            <p>{translate('WELCOME_MODAL.secondStepParagraph2')}</p>

            <ul>
              <li>{translate('WELCOME_MODAL.secondStepOption1')}</li>
              <li>{translate('WELCOME_MODAL.secondStepOption2')}</li>
            </ul>

            <button
              type="button"
              className="rounded-button active"
              onClick={nextStep}
            >
              {translate('WELCOME_MODAL.secondStepAction1')}
            </button>

            <button type="button" className="text-button" onClick={closeModal}>
              {translate('WELCOME_MODAL.secondStepAction2')}
            </button>
          </div>
        ) : null}

        {step === 2 ? (
          <div className="welcome-step">
            <h1>{translate('WELCOME_MODAL.thirdStepTitle')}</h1>

            <p>{translate('WELCOME_MODAL.thirdStepParagraph1')}</p>

            <button
              type="button"
              className="rounded-button active"
              onClick={nextStep}
            >
              {translate('WELCOME_MODAL.thirdStepAction1')}
            </button>

            <button type="button" className="text-button" onClick={closeModal}>
              {translate('WELCOME_MODAL.thirdStepAction2')}
            </button>
          </div>
        ) : null}

        {step === 3 ? (
          <div className="welcome-step">
            <h1>{translate('WELCOME_MODAL.fourthStepTitle')}</h1>

            <p>{translate('WELCOME_MODAL.fourthStepParagraph1')}</p>

            <p
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{
                __html: translate('WELCOME_MODAL.fourthStepParagraph2', {
                  url:
                    '<a class="users-help-url" href="https://users.loyals.club">https://users.loyals.club</a>',
                }),
              }}
            />

            <button
              type="button"
              className="rounded-button active"
              onClick={nextStep}
            >
              {translate('WELCOME_MODAL.fourthStepAction1')}
            </button>
          </div>
        ) : null}
      </div>
    </Modal>
  );
};

export default WelcomeModal;
