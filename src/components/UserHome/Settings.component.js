/** External dependencies */
import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Link from 'next/link';

/** Components */
import UserHomePageContent from './UserHomePageContent.component';
import {
  CustomInput,
  FirstNameSchema,
  LastNameSchema,
} from '../Common/Form.component';
import translate from '../../utils/i18n.utils';

const Settings = ({ userInfo = {}, logout, updateUser, reqError, action }) => {
  const changeOb = {};
  let content = null;
  let schema = null;

  if (action) {
    if (action === 'change-firstname') {
      changeOb.firstName = '';
      schema = FirstNameSchema;
      content = (
        <>
          <label htmlFor="firstName">
            {translate('SETTINGS.changeFirstName')}
          </label>
          <br />
          <Field type="text" name="firstName" component={CustomInput} />
          <ErrorMessage
            name="firstName"
            component="p"
            className="error-field"
          />
        </>
      );
    }

    if (action === 'change-lastname') {
      changeOb.lastName = '';
      schema = LastNameSchema;
      content = (
        <>
          <label htmlFor="lastName">
            {translate('SETTINGS.changeLastName')}
          </label>
          <br />
          <Field type="text" name="lastName" component={CustomInput} />
          <ErrorMessage name="lastName" component="p" className="error-field" />
        </>
      );
    }
  }

  const getContent = () => (
    <div className={`settings-container ${action ? 'active' : ''}`}>
      <div className="settings-step1">
        <h2>{translate('SETTINGS.title')}</h2>

        <div className="settings-menu">
          <p className="settings-menu-item disabled">
            {translate('GENERIC.email')}
            <span className="settings-menu-item-sub">{userInfo.email}</span>
            <i className="icon-front-arrow" />
          </p>

          <Link
            className="settings-menu-item"
            href="/settings/change-firstname"
          >
            <a className="settings-menu-item">
              {translate('GENERIC.firstName')}
              <span className="settings-menu-item-sub">
                {userInfo.firstName}
              </span>
              <i className="icon-front-arrow" />
            </a>
          </Link>

          <Link className="settings-menu-item" href="/settings/change-lastname">
            <a className="settings-menu-item">
              {translate('GENERIC.lastName')}
              <span className="settings-menu-item-sub">
                {userInfo.lastName}
              </span>
              <i className="icon-front-arrow" />
            </a>
          </Link>

          <a
            href="https://users.loyals.club"
            target="_blank"
            rel="noopener noreferrer"
            className="settings-menu-item"
          >
            {translate('GENERIC.help')}
            <i className="icon-front-arrow" />
          </a>

          <button type="button" onClick={logout} className="settings-menu-item">
            {translate('GENERIC.logOut')}
            <i className="icon-front-arrow" />
          </button>
        </div>
      </div>

      <div className="settings-step2">
        {action ? (
          <Formik
            initialValues={changeOb}
            validationSchema={schema}
            onSubmit={(data, { setSubmitting }) => {
              updateUser({ variables: { ...data } });
              setSubmitting(false);
            }}
          >
            {({ isSubmitting }) => (
              <Form>
                <p className="error">{reqError && reqError.text}</p>
                <br />
                <br />
                <div className="input-wrapper">{content}</div>
                <div className="input-wrapper submit">
                  <input
                    type="submit"
                    className="rounded-button active"
                    value="Continuar"
                    disabled={isSubmitting}
                  />
                </div>
              </Form>
            )}
          </Formik>
        ) : null}
      </div>
    </div>
  );

  return (
    <UserHomePageContent
      rootClassName="userprofile-settings"
      getContent={getContent}
    />
  );
};

export default Settings;
