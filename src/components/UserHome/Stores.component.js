/** External dependencies */
import React from 'react';
import Link from 'next/link';

/** Components */
import UserHomePageContent from './UserHomePageContent.component';
import StoreCardComponent from './StoresComponents/StoreCard.component';
import GenericMessageComponent from '../Common/GenericMessage.component';
import translate from '../../utils/i18n.utils';

const Stores = (props) => {
  const { userCustomerProfiles = [] } = props;

  const getContent = () => {
    if (userCustomerProfiles.length > 0) {
      return (
        <>
          <h2>{translate('STORE_LIST.title')}</h2>

          <div className="stores-container">
            {userCustomerProfiles.map((customerProfile) => {
              const { Store } = customerProfile;
              return (
                <Link
                  key={Store.alias}
                  href="/[[...slug]]"
                  as={`/${Store.alias}`}
                  passHref
                >
                  <a>
                    <StoreCardComponent customerProfile={customerProfile} />
                  </a>
                </Link>
              );
            })}
          </div>
        </>
      );
    }

    return (
      <GenericMessageComponent
        type="question"
        message={{
          title: translate('STORE_LIST.emptyTitle'),
          text: translate('STORE_LIST.emptyMessage'),
        }}
      />
    );
  };

  return (
    <UserHomePageContent
      {...props}
      rootClassName={`userstores-content ${
        !userCustomerProfiles.length ? 'no-stores' : ''
      }`}
      getContent={getContent}
    />
  );
};

export default Stores;
