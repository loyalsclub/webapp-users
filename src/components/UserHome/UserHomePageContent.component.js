/** External dependencies */
import React from 'react';

/** Components */
import CrownLoaderComponent from '../Common/CrownLoader.component';
import GenericMessageComponent from '../Common/GenericMessage.component';

const UserHomePageContent = ({
  rootClassName,
  partialData,
  getLoader,
  getError,
  getContent,
  loading,
  error,
}) => {
  // eslint-disable-next-line no-underscore-dangle
  const _getLoader = () => {
    if (!loading) return null;
    if (getLoader) return getLoader();
    return <CrownLoaderComponent />;
  };

  // eslint-disable-next-line no-underscore-dangle
  const _getError = () => {
    if (loading) return null;
    if (!error || partialData) return null;
    if (getError) return getError();

    return <GenericMessageComponent message={error} />;
  };

  // eslint-disable-next-line no-underscore-dangle
  const _getContent = () => {
    if (loading) return null;
    if (error && !partialData) return null;
    return getContent();
  };

  return (
    <div
      className={`
      ${rootClassName}
      ${loading ? ' loading' : ''}
      ${error && !partialData ? ' error' : ''}
    `}
    >
      {_getLoader()}
      {_getError()}
      {_getContent()}
    </div>
  );
};

export default UserHomePageContent;
