import React, { useState } from 'react';
import Modal from 'react-modal';
import notificationsProvider from '../../../providers/notifications/notifications.provider';
import translate from '../../../utils/i18n.utils';

const NotificationsModal = () => {
  const [isOpen, setModalOpen] = useState(true);

  const closeModal = () => {
    notificationsProvider.setSubscriptionBlockedByUser();
    setModalOpen(false);
  };

  const enableNotifications = async () => {
    await notificationsProvider.subscribeUser();
    setModalOpen(false);
  };

  return (
    <Modal
      isOpen={isOpen}
      overlayClassName="notifications-modal-overlay"
      className="notifications-modal-content"
      shouldCloseOnOverlayClick
      onRequestClose={closeModal}
      closeTimeoutMS={300}
    >
      <div className="notifications-modal-header">
        <button
          type="button"
          className="notifications-modal-close-icon"
          onClick={closeModal}
        >
          <i className="icon-cross" />
        </button>
      </div>

      <div className="notifications-modal-body">
        <div className="notification-modal-step">
          <h1>{translate('NOTIFICATIONS_MODAL.title')}</h1>

          <p>{translate('NOTIFICATIONS_MODAL.paragraph')}</p>

          <button
            type="button"
            className="rounded-button active"
            onClick={enableNotifications}
          >
            {translate('GENERIC.accept')}
          </button>

          <button type="button" className="text-button" onClick={closeModal}>
            {translate('GENERIC.block')}
          </button>
        </div>
      </div>
    </Modal>
  );
};

export default NotificationsModal;
