/** External dependencies */
import React, { useEffect, useRef } from 'react';
import QRCode from 'qrcode';

/** Components */
import UserHomePageContent from './UserHomePageContent.component';
import translate from '../../utils/i18n.utils';

const UserProfile = (props) => {
  const qrCanvas = useRef(null);
  const { userInfo, loading, goBack } = props;

  useEffect(() => {
    window.scrollTo(0, 0);

    if (!userInfo) {
      return;
    }

    QRCode.toCanvas(qrCanvas.current, userInfo.alias, {
      errorCorrectionLevel: 'L',
      width: window.innerHeight * 0.5,
      color: {
        dark: '#273248', // Blue dots
        light: '#0000', // Transparent background
      },
    });
  }, [userInfo, loading]);

  const getContent = () => (
    <>
      <button type="button" className="go-back" onClick={goBack}>
        <i className="icon-back-arrow" />
        <span>{translate('GENERIC.back')}</span>
      </button>

      <p className="userprofile-name">
        {userInfo.firstName} {userInfo.lastName}
      </p>

      <p className="userprofile-alias">
        {translate('GENERIC.alias').toUpperCase()}: {userInfo.alias}
      </p>

      <canvas className="userprofile-qr" ref={qrCanvas} />

      <p className="userprofile-message">{translate('USER_PROFILE.message')}</p>
    </>
  );

  return (
    <UserHomePageContent
      {...props}
      rootClassName="userprofile-content"
      getContent={getContent}
    />
  );
};

export default UserProfile;
