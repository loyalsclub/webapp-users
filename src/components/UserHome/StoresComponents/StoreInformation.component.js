/** External dependencies */
import React, { useEffect } from 'react';

/** Utils */
import { useQuery } from '@apollo/client';
import { geo, phone } from '../../../utils/scheme.utils';
import { GET_STORE_INFORMATION } from '../../../queries/store.queries';
import translate from '../../../utils/i18n.utils';

const StoreInformation = ({ alias }) => {
  const { data: { storeInfoByAlias } = {} } =
    useQuery(GET_STORE_INFORMATION, {
      variables: { alias },
    }) || {};

  const { selectedCategory, description, parsedSocialMedia, StoreLocations } =
    storeInfoByAlias || {};

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return !storeInfoByAlias ? null : (
    <div className="store-information">
      <h3>{translate('GENERIC.aboutUs')}</h3>

      <h4>
        <i className={`icon-cat-${selectedCategory.name}`} />
        {selectedCategory.displayName}
      </h4>

      <p className="store-description">{description}</p>

      {parsedSocialMedia && parsedSocialMedia.length ? (
        <div className="social-media-container">
          {parsedSocialMedia.map((socialMedia) => (
            <a
              key={socialMedia.name}
              href={socialMedia.value}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className={`icon-${socialMedia.name}`} />
            </a>
          ))}
        </div>
      ) : null}

      {StoreLocations.map((StoreLocation) => (
        <div key={StoreLocation.id} className="contact-info-container">
          {StoreLocation.email ? (
            <a>
              <i className="icon-envelope" />{' '}
              <span>{StoreLocation.email} </span>
            </a>
          ) : null}

          {StoreLocation.phone ? (
            <a href={phone(StoreLocation.phone)}>
              <i className="icon-phone" /> <span>{StoreLocation.phone} </span>
            </a>
          ) : null}

          {StoreLocation.address ? (
            <a
              href={geo(StoreLocation.geo.coordinates)}
              rel="noopener noreferrer"
              target="_blank"
            >
              <i className="icon-location" />{' '}
              <span>{StoreLocation.address} </span>
            </a>
          ) : null}

          {StoreLocation.website ? (
            <a
              href={StoreLocation.website}
              rel="noopener noreferrer"
              target="_blank"
            >
              <i className="icon-sphere" />
              <span> {StoreLocation.website} </span>
            </a>
          ) : null}
        </div>
      ))}
    </div>
  );
};

export default StoreInformation;
