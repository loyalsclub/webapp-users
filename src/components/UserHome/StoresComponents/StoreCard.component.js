/** External dependencies */
import React from 'react';
import Image from 'next/image';

/** Components */
import StoreLogoComponent from '../../Common/StoreLogo.component';
import translate from '../../../utils/i18n.utils';

const crownByLevel = {
  1: {
    imgSrc: '/img/crown-silver-50.png',
    alt: 'Plata',
  },
  2: {
    imgSrc: '/img/crown-gold-50.png',
    alt: 'Oro',
  },
  3: {
    imgSrc: '/img/crown-safire-50.png',
    alt: 'Zafiro',
  },
};

const StoreCard = ({ customerProfile }) => {
  const { CustomerLevel, Store } = customerProfile;
  const { profileImage, name, category } = Store;

  return (
    <div className="store-card">
      <StoreLogoComponent src={profileImage} className="store-card-logo" />

      <div className="store-card-meta-container">
        <h3>{name}</h3>

        <div className="store-card-secondary-meta-container" />

        <div className="store-card-middle-meta">
          <i className={`icon-cat-${category}`} />
          <span>
            {`${CustomerLevel.activeRewards} ${translate('STORE.rewardCount', {
              count: CustomerLevel.activeRewards,
            })}`}
          </span>
        </div>

        <div className="store-card-lower-meta">
          <div className="store-card-logo">
            <Image
              width={20}
              height={20}
              src={crownByLevel[CustomerLevel.level].imgSrc}
              alt={crownByLevel[CustomerLevel.level].alt}
            />
          </div>

          <span>
            <b>{CustomerLevel.total}</b> / {CustomerLevel.nextLevelPoints}
          </span>
        </div>
      </div>

      <div className="store-card-action-container">
        <i className="icon-front-arrow" />
      </div>
    </div>
  );
};

export default StoreCard;
