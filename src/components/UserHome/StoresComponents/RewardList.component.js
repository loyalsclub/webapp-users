/** External dependencies */
import React from 'react';
import Image from 'next/image';
/** Components */
import RewardCard from './RewardCard.component';
import translate from '../../../utils/i18n.utils';

const LEVEL_SETTINGS = {
  1: {
    crownImage: '/img/crown-silver-50.png',
    alt: 'Plata',
  },
  2: {
    crownImage: '/img/crown-gold-50.png',
    alt: 'Oro',
  },
  3: {
    crownImage: '/img/crown-safire-50.png',
    alt: 'Zafiro',
  },
};

const RewardList = ({
  customerProfileGetStoreLevelsWithRewards,
  currentLevel = null,
}) => {
  if (!customerProfileGetStoreLevelsWithRewards) {
    return null;
  }

  return (
    <div className="reward-level-container">
      <h3>{translate('STORE.rewards')}</h3>

      {customerProfileGetStoreLevelsWithRewards.map(
        ({ level, id, StoreRewards }) => (
          <div key={id} className="reward-level">
            <div className="reward-level-header">
              <Image
                width={25}
                height={25}
                src={LEVEL_SETTINGS[level].crownImage}
                alt={LEVEL_SETTINGS[level].alt}
              />
            </div>

            {StoreRewards.length > 0 ? (
              StoreRewards.map((reward) => (
                <RewardCard
                  StoreReward={reward}
                  key={reward.id}
                  level={level}
                  disabled={currentLevel ? currentLevel < level : false}
                />
              ))
            ) : (
              <p className="no-reward">{translate('STORE.noRewardsOnLevel')}</p>
            )}
          </div>
        )
      )}
    </div>
  );
};

export default RewardList;
