/** External dependencies */
import React from 'react';
import translate from '../../../utils/i18n.utils';

import { getFrequency } from '../../../utils/language.utils';

const RewardCard = ({ StoreReward, level = null, disabled = false }) => (
  <div
    className={`reward-card-container level-${level} ${
      disabled ? 'disabled' : ''
    }`}
  >
    <h4 className="reward-card-title">{StoreReward.title}</h4>

    <div className="reward-card-meta">
      <i className="icon-frequency" />

      <span>
        {getFrequency(StoreReward.UsageFrequency)}

        <span className="reward-frequency-used">
          {StoreReward.isConsideredUsed ? translate('REWARDS.used') : ''}
        </span>
      </span>
    </div>

    <div className="reward-card-description">
      <p>{StoreReward.description}</p>
    </div>

    <div className="reward-card-action">
      {disabled ? <i className="icon-lock" /> : null}
    </div>
  </div>
);

export default RewardCard;
