/** External dependencies */
import React from 'react';
import { useSpring, animated } from 'react-spring';
import Image from 'next/image';
import translate from '../../../utils/i18n.utils';

const LEVEL_SETTINGS = {
  1: {
    gradient: ['#ABB9CF', '#C4D6E7', '#ABB9CF', '#DDEDF9', '#617CA0'],
    crownImage: '/img/crown-silver.png',
    baseColor: '#ABB9CF',
  },
  2: {
    gradient: ['rgba(236,198,53,1)', 'rgba(210, 136, 26, 1)'],
    crownImage: '/img/crown-gold.png',
    baseColor: 'rgba(236,198,53,1)',
  },
  3: {
    gradient: [
      'rgba(0,175,141,1)',
      'rgba(189,255,255,1)',
      'rgba(0,175,141,1)',
      'rgba(189,255,255,1)',
      'rgba(0,175,141,1)',
    ],
    crownImage: '/img/crown-safire.png',
    baseColor: 'rgba(0,175,141,1)',
  },
};

const Progress = ({
  level = 1,
  size = 250,
  total,
  nextLevelPoints,
  currentLevelPoints,
  nextLevelPointsDif,
}) => {
  const SETTINGS = LEVEL_SETTINGS[level];
  // const logoWidth = 0;
  const crownWidth = Math.round(size / 4);
  const strokeWidth = 8; // Width of the stroke
  const iconSize = 12;
  const iconCircleContainerRadius = 12; // Radius of the crown container
  // const topMargin = logoWidth / 2 - iconCircleContainerRadius;
  const topMargin = 0;
  const radius = size / 2 - iconCircleContainerRadius - strokeWidth / 2; // Radius of progress circle

  const centerX = size / 2;
  const centerY = size / 2;

  /** Angle defer by icon */
  // const angleDeferByIconRad = Math.tan(iconCircleContainerRadius / radius);
  const distanceDeferByIcon = 0;

  /** Angle defer by logo */
  // const angleDeferByLogoRad = Math.tan(logoWidth / 2 / radius);
  const distanceDeferByLogo = 0;

  const distanceDeferTotal = distanceDeferByIcon + distanceDeferByLogo;
  const angleDeferTotalRad = 0;

  /** Progress circle */
  const dashArrayLength = 2 * Math.PI * radius;

  const { progress } = useSpring({
    config: { mass: 1, tension: 500, friction: 300 },
    from: { progress: 0 },
    progress:
      level === 3 ? 1 : (total - currentLevelPoints) / nextLevelPointsDif,
  });

  const iconInterpolator = progress.interpolate({
    range: [0, 1],
    output: [
      -Math.PI / 2 + angleDeferTotalRad,
      (3 / 2) * Math.PI - angleDeferTotalRad,
    ],
  });

  return (
    <div className="progress-container" style={{ marginTop: topMargin }}>
      <svg height={size} width={size}>
        <defs>
          <linearGradient
            id="progressGrad"
            x1="0"
            y1="0"
            x2="100%"
            y2="0"
            gradientTransform="rotate(65)"
          >
            {SETTINGS.gradient.map((gradient, index, arr) => (
              <stop
                key={`progressGrad${Math.random()}`}
                offset={(1 / arr.length) * (index + 1)}
                stopColor={gradient}
                stopOpacity="1"
              />
            ))}
          </linearGradient>

          <linearGradient id="circleGrad" x1="0" y1="100%" x2="100%" y2="0">
            {SETTINGS.gradient.map((gradient, index, arr) => (
              <stop
                key={`circleGrad${Math.random()}`}
                offset={(1 / arr.length) * (index + 1)}
                stopColor={gradient}
                stopOpacity="1"
              />
            ))}
          </linearGradient>

          <linearGradient id="circleGradStroke" x1="50%" y1="0" y2="50%" x2="0">
            {SETTINGS.gradient.map((gradient, index, arr) => (
              <stop
                key={`circleGradStroke${Math.random()}`}
                offset={(1 / arr.length) * (index + 1)}
                stopColor={gradient}
                stopOpacity="1"
              />
            ))}
          </linearGradient>
        </defs>

        <circle // Inner grad circle.
          stroke="rgba(35,46,64, 0.1)"
          cx={size / 2} // Center in the middle.
          cy={size / 2} // Center in the middle.
          r={radius - strokeWidth / 4} // Half for inner circle
          strokeWidth={strokeWidth / 2}
          fill="white"
        />

        <circle // Outer grad circle.
          stroke="rgba(35,46,64, 0.2)"
          cx={size / 2}
          cy={size / 2}
          fill="none"
          r={radius + strokeWidth / 4} // Half for outer circle
          strokeWidth={strokeWidth / 2}
        />

        <animated.circle // Progress circle
          cx={centerX}
          cy={centerY}
          fill="none"
          r={radius}
          strokeWidth={strokeWidth}
          transform={`rotate(-90 ${centerX} ${centerY})`}
          stroke="url(#progressGrad)"
          strokeLinecap="round"
          strokeDasharray={dashArrayLength}
          strokeDashoffset={progress.interpolate({
            range: [0, 1],
            output: [dashArrayLength - distanceDeferTotal, distanceDeferTotal],
          })}
        />

        <animated.circle // Progress Icon Container
          r={iconCircleContainerRadius}
          cx={iconInterpolator.interpolate(
            (x) => Math.cos(x) * radius + size / 2
          )}
          cy={iconInterpolator.interpolate(
            (x) => Math.sin(x) * radius + size / 2
          )}
          stroke="url(#circleGradStroke)"
          strokeWidth={4}
          fill="url(#circleGrad)"
        />
      </svg>

      <animated.i // Progress Icon
        className="icon-crown"
        style={{
          color: 'white',
          fontSize: iconSize,
          position: 'absolute',
          top: centerY - iconSize / 2,
          left: centerX - iconSize / 2,
          transform: iconInterpolator
            .interpolate((alpha) => [Math.cos(alpha), Math.sin(alpha)])
            .interpolate((arr) => arr.map((x) => x * radius))
            .interpolate(([x, y]) => `translate(${x}px, ${y}px)`),
        }}
      />

      <div
        className="progress-content"
        style={{
          height: 1.3 * radius,
          width: 1.3 * radius,
          top: '50%',
          transform: 'translateY(-50%)',
        }}
      >
        <div className="progress-content-image-wrapper">
          <Image
            height={64}
            alt={translate('GENERIC.level')}
            src={SETTINGS.crownImage}
            width={crownWidth}
            priority
          />
        </div>

        <p
          className="progress-content-levelindicator"
          style={{
            fontSize: size / 15,
          }}
        >
          {level === 3
            ? translate('CUSTOMER_PROFILE.maxLevel')
            : translate('CUSTOMER_PROFILE.nextLevel')}
        </p>

        <p
          className="progress-content-points"
          style={{
            fontSize: size / 13,
          }}
        >
          <b>{total}</b> / {nextLevelPoints}
        </p>
      </div>
    </div>
  );
};

export default Progress;
