/** External dependencies */
import React from 'react';
import * as Yup from 'yup';

const spanishrex = /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/g;

const CustomInput = ( data ) => {
  const { field, props, form, type, className } = data;
  const classNameArray = className ? [className] : [];

  if(field.value) classNameArray.push('active');
  if(form.errors[field.name] && form.touched[field.name]) classNameArray.push('error');
  return <input className={classNameArray.join(' ')} type={type} {...field} {...props} />;
};

const EmailSchema = Yup.object().shape({
  email: Yup.string()
    .email('Email inválido')
    .required('Requerido')
  });

const today = new Date();
const PersonalInfoSchema = Yup.object().shape({
  fname: Yup.string()
    .required('Requerido')
    .matches(spanishrex, 'Nombre inválido'),
  lname: Yup.string()
    .required('Requerido')
    .matches(spanishrex, 'Apellido inválido'),
  bday: Yup.number()
    .required('Día requerido')
    .min(1, 'Día inválido')
    .max(31, 'Día inválido')
    .positive('Día inválido')
    .integer('Día inválido'),
  bmonth: Yup.number()
    .required('Mes requerido')
    .min(1, 'Mes inválido')
    .max(12, 'Mes inválido')
    .positive('Mes inválido')
    .integer('Mes inválido'),
  byears: Yup.number()
    .required('Año requerido')
    .min(today.getFullYear() - 100, 'Año inválido')
    .max(today.getFullYear(), 'Año inválido')
    .integer('Año inválido')
});

const FirstNameSchema = Yup.object().shape({
  firstName: Yup.string()
    .required('Requerido')
    .matches(spanishrex, 'Nombre inválido')
});

const LastNameSchema = Yup.object().shape({
  lastName: Yup.string()
    .required('Requerido')
    .matches(spanishrex, 'Apellido inválido')
});


const PasswordSchema = Yup.object().shape({
  password: Yup.string().min(6, 'Mínimo 6 caracteres').required("Requerido"),
  repeatpassword: Yup.string().required('Requerido').when("password", {
    is: val => (val && val.length > 0 ? true : false),
    then: Yup.string().oneOf(
      [Yup.ref("password")],
      "Las contraseñas deben coincidir"
    )
  })
});

export { CustomInput, FirstNameSchema, LastNameSchema, PersonalInfoSchema, PasswordSchema, EmailSchema };
