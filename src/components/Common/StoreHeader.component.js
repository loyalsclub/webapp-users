import React from 'react';
import translate from '../../utils/i18n.utils';

/** Components */
import StoreLogoComponent from './StoreLogo.component';

const StoreHeader = ({ activeTab, onChangeTab, Store }) => (
  <div className="nav">
    <div className="store-header">
      <div className="store-header-logo">
        <StoreLogoComponent src={Store.profileImage} />
      </div>

      <div className="store-button-tabs">
        <button
          type="button"
          onClick={() => onChangeTab(1)}
          className={`${activeTab === 1 ? 'active' : ''}`}
        >
          {translate('STORE.rewards')}
        </button>

        <button
          type="button"
          onClick={() => onChangeTab(2)}
          className={`${activeTab === 2 ? 'active' : ''}`}
        >
          {translate('GENERIC.us')}
        </button>
      </div>
    </div>
  </div>
);

export default StoreHeader;
