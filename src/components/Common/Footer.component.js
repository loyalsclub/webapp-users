import React from 'react';
import translate from '../../utils/i18n.utils';

const Footer = React.forwardRef((props, ref) => (
  <footer ref={ref} className="footer">
    <p className="footer-copyright">© {new Date().getFullYear()} Loyals.club</p>

    <div className="footer-link-container">
      <a
        className="footer-link"
        href="https://users.loyals.club"
        target="_blank"
        rel="noopener noreferrer"
      >
        {translate('FOOTER.howItWorks')}
      </a>

      <a
        className="footer-link"
        href="https://partners.loyals.club"
        target="_blank"
        rel="noopener noreferrer"
      >
        {translate('FOOTER.becomePartner')}
      </a>

      <a
        className="footer-link"
        href="https://users.loyals.club/privacy-policy.html"
        target="_blank"
        rel="noopener noreferrer"
      >
        {translate('FOOTER.privacyPolicy')}
      </a>

      <a className="footer-link" href="mailto:info@loyals.club">
        info@loyals.club
      </a>
    </div>
  </footer>
));

export default Footer;
