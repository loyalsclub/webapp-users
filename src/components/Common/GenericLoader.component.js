/** External dependencies */
import React from 'react';

const GenericLoader = ({ className }) => (
  <div className={`lds-spinner ${className}`}>
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
  </div>
);

export default GenericLoader;
