/** External dependencies */
import React from 'react';
import Image from 'next/image';
import translate from '../../utils/i18n.utils';

const messageImages = {
  error: '/img/common/error-crown@2x.png',
  question: '/img/common/cloud-question.png',
};

const GenericMessage = ({ type = 'error', message }) => (
  <div className="genericmessage-container">
    <div className="genericmessage-image-wrapper">
      <Image
        width={180}
        height={175}
        src={messageImages[type]}
        alt="Message!"
      />
    </div>

    <h2>
      {message && message.title
        ? message.title
        : translate('ERRORS.exclamation')}
    </h2>

    <p>
      {message && message.text ? message.text : translate('ERRORS.default')}
    </p>
  </div>
);

export default GenericMessage;
