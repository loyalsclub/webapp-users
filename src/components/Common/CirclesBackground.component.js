import React from 'react';

const CirclesBackground = () => (
  <div className="circles-background">
    <div className="upper" />
    <div className="lower" />
  </div>
);

export default CirclesBackground;
