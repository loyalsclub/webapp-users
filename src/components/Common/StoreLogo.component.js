/** External dependencies */
import React, { useState } from 'react';
import Image from 'next/image';
import GenericLoaderComponent from './GenericLoader.component';

const StoreLogo = ({ src, style }) => {
  const [logoLoaded, setLogoLoaded] = useState(false);
  const [logoError, setLogoError] = useState(false);

  return (
    <div className="storelogo-container" style={style}>
      <div className={`storelogo ${!logoLoaded || logoError ? 'hidden' : ''}`}>
        <Image
          src={src}
          onLoad={() => setLogoLoaded(true)}
          alt="Logo"
          onError={() => {
            setLogoLoaded(true);
            setLogoError(true);
          }}
          layout="responsive"
          width={70}
          height={70}
        />
      </div>

      {logoLoaded && logoError ? (
        <div className="storelogo">
          <Image
            src="/img/store-placeholder.jpg"
            alt="Alternate Logo"
            width={70}
            height={70}
          />
        </div>
      ) : null}

      {!logoLoaded ? <GenericLoaderComponent className="store-loader" /> : null}
    </div>
  );
};

export default StoreLogo;
