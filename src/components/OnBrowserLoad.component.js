import { useEffect } from 'react';
import { deviceMaxSize } from '../utils/device.utils';

const OnBrowserLoad = () => {
  useEffect(function initListeners() {
    window.addEventListener('load', () => {
      const vh = window.innerHeight * 0.01;
      // Then we set the value in the --vh custom property to the root of the document
      document.documentElement.style.setProperty('--vh', `${vh}px`);

      setTimeout(() => {
        const vhRefreshed = window.innerHeight * 0.01;
        // Then we set the value in the --vh custom property to the root of the document
        document.documentElement.style.setProperty('--vh', `${vhRefreshed}px`);
      }, 400);
    });

    // We listen to the resize event
    window.addEventListener('orientationchange', () => {
      // We execute the same script as before
      setTimeout(() => {
        const vh = window.innerHeight * 0.01;
        document.documentElement.style.setProperty('--vh', `${vh}px`);
      }, 500); // We add set time out to trigger the update after orientationchanged finished.
    });

    window.addEventListener('resize', () => {
      if (window.innerWidth < deviceMaxSize.tablet) return;

      // We execute the same script as before
      const vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
    });
  }, []);

  return null;
};

export default OnBrowserLoad;
