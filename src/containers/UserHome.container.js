/** External dependencies */
import React, { useEffect, useState } from 'react';

import { useQuery } from '@apollo/client';
import WelcomeModalComponent from '../components/UserHome/WelcomeModal.component';
import CirclesBackground from '../components/Common/CirclesBackground.component';
import FooterComponent from '../components/Common/Footer.component';
import notificationsProvider from '../providers/notifications/notifications.provider';
import { USER_INFO } from '../queries/user.queries';
import NotificationsModal from '../components/UserHome/NotificationsModal/NotificationsModal.component';

const UserHomeContainer = ({ content }) => {
  const [isOpenWelcomeModal, setWelcomeModalOpen] = useState(false);
  const closeWelcomeModal = () => setWelcomeModalOpen(false);

  const { data: { userInfo = {} } = {} } = useQuery(USER_INFO, {
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  useEffect(function initWelcomeModal() {
    const showModal = localStorage.getItem('welcome-user-modal');

    if (!showModal) {
      localStorage.setItem('welcome-user-modal', true);
      setWelcomeModalOpen(true);
    }
  }, []);

  useEffect(
    function initWebPushSubscription() {
      const isUserLoaded = Boolean(userInfo.id);

      if (isUserLoaded) {
        notificationsProvider.initSubscriptionStatus(
          userInfo.webPushSubscription
        );
      }
    },
    [userInfo.id, userInfo.webPushSubscription]
  );

  return (
    <div className="userhome-cont">
      <WelcomeModalComponent
        isOpen={isOpenWelcomeModal}
        closeModal={closeWelcomeModal}
      />

      {!isOpenWelcomeModal && notificationsProvider.showAskForPermission() && (
        <NotificationsModal />
      )}

      <div className="upper-content">
        <CirclesBackground />
        {content}
      </div>

      <FooterComponent />
    </div>
  );
};

export default UserHomeContainer;
