import React from 'react';
import PropTypes from 'prop-types';
import { useInView } from 'react-intersection-observer';
import Image from 'next/image';
import CrownLoaderComponent from '../components/Common/CrownLoader.component';
import CirclesBackground from '../components/Common/CirclesBackground.component';
import FooterComponent from '../components/Common/Footer.component';

import SliderComponent from '../components/AuthHome/Slider.component';

const propTypes = {
  content: PropTypes.node.isRequired,
};

const AuthHomeContaiener = ({
  storeData,
  content,
  isExact,
  loading,
  split,
}) => {
  const { ref, inView } = useInView({
    threshold: 0,
  });

  return (
    <div
      className={`authhome-cont ${split ? 'split' : 'no-split'} ${
        storeData ? 'from-store' : ''
      } ${!isExact ? 'not-exact' : 'exact'} ${loading ? 'loading' : ''}`}
    >
      {loading ? (
        <CrownLoaderComponent />
      ) : (
        <>
          <div className={`upper-content ${inView ? 'footer-visible' : ''}`}>
            {split ? (
              <>
                <div className="col left">
                  <SliderComponent storeData={storeData} />
                </div>

                <div className="col right">
                  <div className="authhome-logo">
                    <Image
                      width={120}
                      height={35}
                      src="/img/logo.png"
                      alt="Loyals.Club"
                    />
                  </div>

                  <div className="authhome-logo-mobile">
                    <Image
                      width={120}
                      height={35}
                      src="/img/logo-blue.png"
                      alt="Loyals.Club"
                    />
                  </div>
                  {content}
                </div>
              </>
            ) : (
              <div className="col">
                <CirclesBackground />
                {content}
              </div>
            )}
          </div>

          <FooterComponent ref={ref} />
        </>
      )}
    </div>
  );
};

AuthHomeContaiener.propTypes = propTypes;

export default AuthHomeContaiener;
