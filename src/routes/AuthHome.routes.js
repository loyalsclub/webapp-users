import React, { useEffect, useState, useCallback } from 'react';
import { withApollo } from '@apollo/client/react/hoc';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { useLazyQuery } from '@apollo/client';
import { useRouter } from 'next/router';

/** Providers */
import { withAuthentication } from '../providers/Authentication';
import { parseError } from '../utils/errors.utils';
import {
  getQueryString,
  getSlugPiecesFromPath,
  getStoreFromUrl,
  isStoreUrl,
} from '../utils/scheme.utils';

/** Containers */
import AuthHomeContainer from '../containers/AuthHome.container';

/** Pages */
import LoginPage from '../pages/AuthHome/Login.page';
import RegisterPage from '../pages/AuthHome/Register.page';
import GeneralAuthPage from '../pages/AuthHome/GeneralAuth.page';
import RecoverPasswordPage from '../pages/AuthHome/RecoverPassword.page';
import CompleteProfilePage from '../pages/AuthHome/CompleteProfile.page';
import StoreInfoPage from '../pages/AuthHome/StoreInfo.page';

/** Components */
import GenericMessageComponent from '../components/Common/GenericMessage.component';

/** Queries */
import { IS_VALID_USER, STORE_INFO } from '../queries/user.queries';

const AuthHomeRoutes = ({ authObj, client, setAuth, logOut, isExact }) => {
  const router = useRouter();
  const queryString = getQueryString(router.asPath);

  const [error, setError] = useState(null);
  const [storeData, setStoreData] = useState(null);

  const redirectTo = useCallback(
    (pathname) => () =>
      router.push({
        pathname,
        search: queryString,
      }),
    [router, queryString]
  );

  const [getStoreInfo] = useLazyQuery(STORE_INFO, {
    notifyOnNetworkStatusChange: true,
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      if (!data.storeInfoByAlias) return;

      setStoreData(data.storeInfoByAlias);
    },
  });

  const checkForStoreRedirect = useCallback(() => {
    const urlParams = new URLSearchParams(queryString);
    const redirectUrl = urlParams.get('redirect-url');

    if (!redirectUrl) return;
    const path = redirectUrl.split('?');
    const alias = getStoreFromUrl(path[0]);

    if (!alias) return;

    getStoreInfo({ variables: { alias } });
  }, [getStoreInfo, queryString]);

  // We need two use effect hooks, if we add location in the second one
  // we will get into an infinite loop redirect. But we need it on the first
  // one because we need an updated search location.
  useEffect(() => {
    const unsubscribe = authObj.auth.onAuthStateChanged(async (user) => {
      if (!user) {
        checkForStoreRedirect();
      }
    });
    return function cleanup() {
      unsubscribe();
    };
  }, [authObj.auth, checkForStoreRedirect]);

  useEffect(() => {
    const unsubscribe = authObj.auth.onAuthStateChanged(async (user) => {
      let data;
      if (!user) return;
      try {
        ({ data } = await client.query({
          query: IS_VALID_USER,
          fetchPolicy: 'cache-first',
        }));
      } catch (err) {
        setError(parseError(err));
      }

      const isAlreadyOnCompleteProfile =
        router.query &&
        router.query.slug &&
        router.query.slug.length &&
        router.query.slug[0] === 'complete-profile';

      const redirectToCompleteProfile =
        data && !data.isValidUser && !isAlreadyOnCompleteProfile;

      if (redirectToCompleteProfile) {
        return redirectTo('/complete-profile')();
      }
    });
    return function cleanup() {
      unsubscribe();
    };
  }, [authObj.auth, client, redirectTo, router.query]);

  const renderBasedOnSlug = () => {
    const routeSlug = getSlugPiecesFromPath(router.asPath) || [];

    if (isExact) {
      return <GeneralAuthPage redirectTo={redirectTo} storeData={storeData} />;
    }

    const firstPiece = [...routeSlug].shift();

    switch (firstPiece) {
      case 'complete-profile':
        return <CompleteProfilePage logOut={logOut} setAuth={setAuth} />;
      case 'recover-password':
        return <RecoverPasswordPage />;
      case 'register':
        return <RegisterPage />;
      case 'login':
        return <LoginPage />;
      default:
        return <StoreInfoPage />;
    }
  };

  return (
    <AuthHomeContainer
      isExact={isExact}
      error={error}
      split={!isStoreUrl(router.asPath)}
      storeData={storeData}
      content={
        <TransitionGroup className="authhome-tg">
          {error ? (
            <GenericMessageComponent message={error} />
          ) : (
            <CSSTransition
              timeout={{ enter: 300, exit: 300 }}
              classNames="fade500"
            >
              {renderBasedOnSlug()}
            </CSSTransition>
          )}
        </TransitionGroup>
      }
    />
  );
};

export default withAuthentication(withApollo(AuthHomeRoutes));
