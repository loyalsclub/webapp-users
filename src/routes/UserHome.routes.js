import React, { useCallback, useEffect } from 'react';
import { useRouter } from 'next/router';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

/** Providers */
import { withAuthentication } from '../providers/Authentication';

/** Containers */
import UserHomeContainer from '../containers/UserHome.container';

/** Pages */
import UserProfilePage from '../pages/UserHome/UserProfile.page';
import SettingsPage from '../pages/UserHome/Settings.page';
import CustomerProfilePage from '../pages/UserHome/CustomerProfile.page';
import StoresPage from '../pages/UserHome/Stores.page';

/** Utils */
import {
  getQueryString,
  getSlugPiecesFromPath,
  getStoreFromUrl,
} from '../utils/scheme.utils';

const UserHomeRoutes = ({ logOut, isExact }) => {
  const router = useRouter();
  const queryString = getQueryString(router.asPath);

  const redirectToSettings = useCallback(() => router.push('/settings'), [
    router,
  ]);

  const checkForRedirect = useCallback(() => {
    const urlParams = new URLSearchParams(queryString);
    const redirect = urlParams.get('redirect-url');

    if (redirect) return router.push(redirect);

    const alias = getStoreFromUrl(router.asPath);
    if (!alias && router.asPath !== '/') {
      return router.push('/');
    }
  }, [router, queryString]);

  useEffect(() => {
    checkForRedirect();
  }, [checkForRedirect]);

  const renderBasedOnSlug = () => {
    const routeSlug = getSlugPiecesFromPath(router.asPath) || [];

    if (isExact) {
      return <StoresPage />;
    }

    const firstPiece = routeSlug.shift();

    switch (firstPiece) {
      case 'login':
        router.push('/');
        return <div />;
      case 'settings':
        return (
          <SettingsPage
            redirectToSettings={redirectToSettings}
            logout={logOut}
            action={routeSlug.shift()}
          />
        );
      case 'p':
        return <UserProfilePage />;
      default:
        return <CustomerProfilePage alias={firstPiece} />;
    }
  };

  return (
    <UserHomeContainer
      content={
        <TransitionGroup className="userhome-tg">
          <CSSTransition timeout={{ enter: 600, exit: 0 }} classNames="fade200">
            {renderBasedOnSlug()}
          </CSSTransition>
        </TransitionGroup>
      }
    />
  );
};

export default withAuthentication(UserHomeRoutes);
