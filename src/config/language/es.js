export default {
  GENERIC: {
    continue: 'Continuar',
    email: 'Email',
    firstName: 'Nombre',
    lastName: 'Apellido',
    alias: 'Alias',
    birthDate: 'Nacimiento',
    day: 'Día',
    month: 'Mes',
    year: 'Año',
    password: 'Contraseña',
    login: 'Iniciar Sesión',
    us: 'Nosotros',
    aboutUs: 'Sobre nosotros',
    join: 'Unirme',
    accept: 'Aceptar',
    block: 'Bloquear',
    help: 'Ayuda',
    logOut: 'Cerrar Sesión',
    back: 'Volver',
    level: 'Nivel',
  },

  ERRORS: {
    emailAlreadyRegistered:
      'Ups, parece que el email ya se encuentra registrado.',
    exclamation: '¡Ups!',
    default: 'Parece que hubo un error.',
    storeNotFound: 'No se encuentra el comercio pedido.',
    generic: 'Parece que hubo un problema. Probá nuevamente por favor.',
    server:
      'Parece que hay un problema con nuestro sistema. \n Por favor probá nuevamente más tarde o comunicate con info@loyals.club',
    network: 'Parece que hay un error en la red. \n Chequeá tu conectividad.',
    apiUserIsNotAuthorized: 'No está autorizado para realizar la acción.',
    apiUserNotFound: 'Usuario no encontrado',
    apiUserIsNotACustomer:
      'Parece que todavía no sos parte del Club de Clientes de este comercio. \n Pediles que te inviten presentando tu perfil.',
    apiTransactionNotFound: 'No se reconoce el código del cupón',
    apiTransactionRedeemed: 'Este cupón ya fue utilizado',
    firebaseUserNotFound: 'Usuario o contraseña incorrectos',
    firebaseWrongPassword: 'Usuario o contraseña incorrectos',
    firebaseInvalidEmail: 'Email con formato inválido',
  },

  SLIDER: {
    firstSlideDescription:
      'Vinculate gratis con tus comercios favoritos en Loyals.club',
    firstSlideAlt: '¡Vinculate!',
    secondSlideDescription:
      'Presentá tu perfil cuando consumas y subí tu nivel',
    secondSlideAlt: '¡Consumí!',
    thirdSlideDescription:
      'Disfrutá de beneficios exclusivos cada vez que vuelvas',
    thirdSlideAlt: '¡Disfrutá!',
  },

  FOOTER: {
    howItWorks: '¿Cómo funciona?',
    becomePartner: 'Quiero ser Partner',
    privacyPolicy: 'Política de Privacidad',
  },

  GENERAL_AUTH: {
    logoAlt: 'Logo',
    ticketGuyAlt: 'Welcome',
    loginWith: 'Ingresar con:',
    register: '¡Unirme!',
  },

  REGISTER: {
    atLeastChars: 'Mínimo 6 caracteres',
    repeatPassword: 'Repetir contraseña',
  },

  COMPLETE_PROFILE: {
    intro:
      '¡Hola! Necesitamos que completes unos datos para disfrutar tu experiencia Loyals al máximo.',
    email: 'Email de contacto',
    logOut: 'Salir de mi cuenta',
  },

  LOGIN: {
    forgotPassword: '¿Olvidaste tu contraseña?',
  },

  RECOVER_PASSWORD: {
    askForEmail: 'Ingresá el correo electrónico con el que te registraste.',
    recoverButton: 'Recuperar Contraseña',
    emailSent:
      'Se ha enviado un email con un enlace para recuperar la contraseña.',
  },

  STORE: {
    rewards: 'Beneficios',
    rewardCount: {
      one: 'beneficio',
      other: 'beneficios',
    },
    noRewardsOnLevel: 'Todavía no hay beneficios en este nivel',
  },

  REWARDS: {
    used: '- USADO',
  },

  STORE_REFERRAL_MODAL: {
    title: '¡Felicitaciones!',
    invited:
      'Desde %{storeName} te invitamos a ser parte de nuestro Club de Clientes.',
    register:
      'Registrate o inicia sesión y comenzá a disfrutar de todo lo que tenemos preparado exclusivamente para vos.',
  },

  WELCOME_MODAL: {
    firstStepTitle: '¡Hola!',
    firstStepParagraph1: 'Te damos la bienvenida a nuestro Club de Clientes.',
    firstStepParagraph2: 'La plataforma es muy fácil de usar.',
    firstStepAction1: 'Quiero saber cómo funciona',
    firstStepAction2: 'Ya se usarla',
    secondStepTitle: 'Sumá puntos',
    secondStepParagraph1:
      'Cada vez que consumas, te sumaremos los puntos correspondientes a tu compra.',
    secondStepParagraph2: 'Esto puede ser:',
    secondStepOption1: 'Con un mensaje de WhatsApp o Instagram.',
    secondStepOption2: 'Presencialmente.',
    secondStepAction1: 'Subí de nivel',
    secondStepAction2: 'Ya se usarla',
    thirdStepTitle: 'Subí de nivel',
    thirdStepParagraph1:
      'A medida que sumes puntos, mejorarás tu nivel y con él tus beneficios.',
    thirdStepAction1: '¿Cómo uso mis beneficios?',
    thirdStepAction2: 'Ya se usarla',
    fourthStepTitle: 'Tus beneficios',
    fourthStepParagraph1:
      'Cuando quieras usar un beneficio presentanos tu credencial digital o indicanos tu ALIAS.',
    fourthStepParagraph2:
      'Si tenés alguna duda podes ir a %{url} y con gusto te vamos a ayudar.',
    fourthStepAction1: 'Comenzar',
  },

  NOTIFICATIONS_MODAL: {
    title: '¿Te gustaría recibir notificaciones?',
    paragraph:
      'Mantenete actualizado con los puntos que sumás y los últimos beneficios disponibles.',
  },

  STORE_LIST: {
    title: 'Mis Comercios',
    emptyTitle: 'Todavía no tenés comercios',
    emptyMessage:
      'Acercate a cualquier comercio adherido a Loyals.club y sumate a su club de clientes presentando tu perfil.',
  },

  SETTINGS: {
    title: 'Configuración',
    changeFirstName: 'Cambiar nombre',
    changeLastName: 'Cambiar apellido',
  },

  USER_PROFILE: {
    message:
      'Presentá este QR o indicá tu ALIAS para sumar puntos y canjear beneficios.',
  },

  CUSTOMER_PROFILE: {
    pointsLoaded: {
      one: '¡Felicitaciones! Sumaste %{pointsLoaded} punto',
      other: '¡Felicitaciones! Sumaste %{pointsLoaded} puntos',
    },
    pointsError: 'Ups, parece que ese cupón ya fue usado o no es válido',
    maxLevel: '¡Máximo Nivel!',
    nextLevel: 'Siguiente Nivel',
  },
};
