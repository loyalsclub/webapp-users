const firebaseConfig = require('./firebase.config.json');

module.exports = {
  API_URL: process.env.NEXT_PUBLIC_API_URL,
  FIREBASE_CONFIG: firebaseConfig[process.env.NODE_ENV],
  SENTRY_API: process.env.NEXT_PUBLIC_SENTRY_API,
  ANALYTICS: {
    PROVIDER: process.env.NEXT_PUBLIC_ANALYTICS_PROVIDER,
    API_KEY: process.env.NEXT_PUBLIC_ANALYTICS_API_KEY,
  },
  DEBUGGING_METRICS: process.env.NEXT_PUBLIC_DEBUGGING_METRICS === 'true',
  ENABLE_REPORTING: process.env.NEXT_PUBLIC_ENABLE_REPORTING === 'true',
  ENABLE_GOOGLE_ANALYTICS: process.env.NODE_ENV === 'production',
};
