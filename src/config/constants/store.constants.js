export const SOCIAL_MEDIAS_DOMAIN = {
  twitter: 'twitter.com',
  facebook: 'facebook.com',
  instagram: 'instagram.com',
  whatsapp: 'wa.me',
};

export const USAGE_FREQUENCIES = {
  FOREVER: 'forever',
  ONE_TIME: 'oneTime',
  EVERY_CERTAIN_DAYS: 'everyCertainDays',
  EVERY_CERTAIN_HOURS: 'everyCertainHours',
  CERTAIN_WEEK_DAYS: 'certainWeekDays',
};
