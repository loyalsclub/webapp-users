import StoreTypePolicies from './store.type-policies';
import StoreCategoryTypePolicies from './store-category.type-policies';
import StoreCountryTypePolicies from './store-country.type-policies';

export default {
  Store: StoreTypePolicies,
  StoreCategory: StoreCategoryTypePolicies,
  StoreCountry: StoreCountryTypePolicies,
};
