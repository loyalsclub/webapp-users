import { gql } from '@apollo/client';
import {
  addCountryCodeToNumber,
  addSocialMediaDomain,
} from '../../../utils/store.utils';

export default {
  fields: {
    selectedCategory: {
      read(_, { cache, readField }) {
        const categoryName = readField('category');

        const selectedCategory = cache.readFragment({
          id: cache.identify({
            __typename: 'StoreCategory',
            name: categoryName,
          }),
          fragment: gql`
            fragment CategoryDisplayName on StoreCategory {
              name
              displayName
            }
          `,
        }) || { name: 'other', displayName: 'Otros' };

        return selectedCategory;
      },
    },
    parsedSocialMedia: {
      read(_, { cache, readField }) {
        const socialMedias = readField('socialMedia') || {};

        return Object.keys(socialMedias).map((name) => {
          let socialMediaValue = socialMedias[name];

          const isWhatsapp = name === 'whatsapp';

          if (isWhatsapp) {
            const countryCode = readField('countryCode');

            const { areaCode } =
              cache.readFragment({
                id: cache.identify({
                  __typename: 'StoreCountry',
                  name: countryCode,
                }),
                fragment: gql`
                  fragment CountryCode on StoreCountry {
                    areaCode
                  }
                `,
              }) || {};

            if (areaCode) {
              socialMediaValue = addCountryCodeToNumber(
                socialMediaValue,
                areaCode
              );
            }
          }

          const parsedValue = addSocialMediaDomain(name, socialMediaValue);

          return { name, value: parsedValue };
        });
      },
    },
  },
};
