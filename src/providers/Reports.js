import * as Sentry from '@sentry/react';
import { Integrations } from '@sentry/tracing';
import ENV from '../config/env.config';

const useReporting = ENV.ENABLE_REPORTING;

const initialize = () => {
  if (!useReporting) return;
  Sentry.init({
    dsn: ENV.SENTRY_API,
    integrations: [new Integrations.BrowserTracing()],
    environment: process.env.NODE_ENV,
    debug: process.env.NODE_ENV === 'development',

    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    tracesSampleRate: 1.0,
  });
};

const captureException = (ErrorObj) => {
  if (!useReporting) return;
  Sentry.captureException(ErrorObj);
};

export default { initialize, captureException };
