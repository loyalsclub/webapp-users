import React from 'react';
import firebase from 'firebase';
import app from 'firebase/app';
import 'firebase/auth';
import envConfig from '../config/env.config';

export default class Authentication {
  constructor() {
    if (!Authentication.init) {
      if (!firebase.apps.length) {
        app.initializeApp(envConfig.FIREBASE_CONFIG);
      }

      Authentication.init = true;
    }

    this.auth = app.auth();
    this.auth.useDeviceLanguage();
  }

  getCurrentUser = () => (this.auth ? this.auth.currentUser : null);

  doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doSignInWithGoogle = () => {
    const provider = new app.auth.GoogleAuthProvider();
    return this.auth.signInWithRedirect(provider);
  };

  doSignInWithFacebook = () => {
    const provider = new app.auth.FacebookAuthProvider();
    return this.auth.signInWithRedirect(provider);
  };

  doSignInWithCustomToken = (loginToken) =>
    this.auth.signInWithCustomToken(loginToken);

  recoverPassword = (email) => this.auth.sendPasswordResetEmail(email);

  doSignOut = () => {
    this.auth.signOut();
  };

  doPasswordReset = (email) => this.auth.sendPasswordResetEmail(email);
}

export const AuthenticationContext = React.createContext(null);

export const withAuthentication = (Component) => (props) => (
  <AuthenticationContext.Consumer>
    {(authObj) => <Component {...props} authObj={authObj} />}
  </AuthenticationContext.Consumer>
);
