import { urlB64ToUint8Array } from '../../utils/notifications.utils';
import { getApolloClient } from '../Apollo';
import { UPDATE_USER_WEB_PUSH_SUBSCRIPTION } from '../../queries/user.queries';

const storageKey = 'notification-permission';

class NotificationProvider {
  constructor() {
    this.applicationServerPublicKey =
      process.env.NEXT_PUBLIC_APP_SERVER_PUBLIC_KEY;
  }

  initialize() {
    this.notificationPermission = localStorage.getItem(storageKey);

    if (NotificationProvider.isSupported()) {
      const config = new URLSearchParams({
        applicationServerPublicKey: this.applicationServerPublicKey,
      }).toString();

      this.registerWorkerPromise = navigator.serviceWorker
        .register(`notifications-worker.js?${config}`)
        .catch((error) => {
          console.error('Service Worker Error', error);
        });
    } else {
      console.warn('Push messaging is not supported');
    }
  }

  static isSupported() {
    return (
      'serviceWorker' in navigator &&
      'PushManager' in window &&
      'Notification' in window
    );
  }

  isInitialized() {
    return Boolean(this.registerWorkerPromise);
  }

  getPushManager() {
    return this.registerWorkerPromise.then(({ pushManager }) => pushManager);
  }

  initSubscriptionStatus(currentSubscription) {
    if (!NotificationProvider.isSupported() || !this.isInitialized()) {
      return;
    }

    return this.getPushManager()
      .then((pushManager) => pushManager.getSubscription())
      .then((subscription) => {
        const hasSubscriptionChanged =
          Boolean(currentSubscription) !== Boolean(subscription);

        const hasEndpointChanged =
          (currentSubscription && currentSubscription.endpoint) !==
          (subscription && subscription.endpoint);

        if (hasSubscriptionChanged || hasEndpointChanged) {
          this.updateSubscriptionOnServer(subscription);
        }

        const hasBeenChangedFromSettings =
          this.notificationPermission &&
          window.Notification.permission !== this.notificationPermission;

        const isAllowingNotifications =
          window.Notification.permission !== 'denied';

        if (hasBeenChangedFromSettings && isAllowingNotifications) {
          this.subscribeUser();
        }
      });
  }

  updateSubscriptionOnServer(subscription) {
    const client = getApolloClient();

    if (client) {
      client.mutate({
        mutation: UPDATE_USER_WEB_PUSH_SUBSCRIPTION,
        variables: { subscription },
      });
    }

    this.setNotificationPermission(window.Notification.permission);
  }

  subscribeUser() {
    if (!NotificationProvider.isSupported() || !this.isInitialized()) {
      return;
    }

    const applicationServerKey = urlB64ToUint8Array(
      this.applicationServerPublicKey
    );

    return this.getPushManager()
      .then((pushManager) =>
        pushManager.subscribe({
          userVisibleOnly: true,
          applicationServerKey,
        })
      )
      .then((subscription) => this.updateSubscriptionOnServer(subscription))
      .catch((err) => {
        console.debug('Failed to subscribe the user: ', err);
      });
  }

  unsubscribeUser() {
    if (!NotificationProvider.isSupported() || !this.isInitialized()) {
      return;
    }

    return this.getPushManager()
      .then((pushManager) => pushManager.getSubscription())
      .then((subscription) => subscription && subscription.unsubscribe())
      .catch((error) => {
        console.debug('Error unsubscribing', error);
      })
      .then(() => this.updateSubscriptionOnServer(null));
  }

  setSubscriptionBlockedByUser() {
    this.setNotificationPermission('denied');
  }

  setNotificationPermission(permission) {
    this.notificationPermission = permission;

    localStorage.setItem(storageKey, this.notificationPermission);
  }

  showAskForPermission() {
    return (
      NotificationProvider.isSupported() &&
      window.Notification.permission === 'default' &&
      this.notificationPermission !== 'denied'
    );
  }
}

export default new NotificationProvider();
