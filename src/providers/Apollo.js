import { useMemo } from 'react';
import firebase from 'firebase';
import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
  Observable,
} from '@apollo/client';
import ENV from '../config/env.config';
import typeDefs from '../queries/types';
import typePolicies from './apollo/type-policies';

let apolloClient;

function getAuthTokenIfLogged() {
  return new Promise((resolver) => {
    const subscription = firebase.auth().onAuthStateChanged(async (user) => {
      if (!user) return resolver();

      const token = await user.getIdToken();
      subscription();
      return resolver(token);
    });
  });
}

async function request(operation) {
  const token = await getAuthTokenIfLogged();

  if (token) {
    operation.setContext({
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }
}

const requestLink = new ApolloLink(
  (operation, forward) =>
    new Observable((observer) => {
      let handle;

      Promise.resolve(operation)
        .then(request)
        .then(() => {
          handle = forward(operation).subscribe({
            next: observer.next.bind(observer),
            error: observer.error.bind(observer),
            complete: observer.complete.bind(observer),
          });
        })
        .catch(observer.error.bind(observer));

      return () => {
        if (handle) handle.unsubscribe();
      };
    })
);

function createApolloClient() {
  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: ApolloLink.from([
      requestLink,
      new HttpLink({
        uri: ENV.API_URL,
      }),
    ]),
    typeDefs,
    cache: new InMemoryCache({ typePolicies }),
  });
}

export function initializeApollo(initialState = null) {
  const newApolloClient = apolloClient ?? createApolloClient();

  if (initialState) {
    const existingCache = newApolloClient.extract();

    newApolloClient.cache.restore({ ...existingCache, ...initialState });
  }

  if (!apolloClient) {
    apolloClient = newApolloClient;
  }

  return newApolloClient;
}

export function useApollo(initialState) {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);

  return store;
}

export const getApolloClient = () => apolloClient;
