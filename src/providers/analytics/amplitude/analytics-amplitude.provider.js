import { ANALYTICS } from '../../../config/env.config';

export default class AmplitudeAnalytics {
  constructor() {
    this.isInitialized = false;
    this.instance = {
      logEvent: () => {},
      setUserId: () => {},
      identify: () => {},
      regenerateDeviceId: () => {},
    };
  }

  initialize() {
    const amplitude = require('amplitude-js');
    this.instance = amplitude.getInstance();
    this.instance.init(ANALYTICS.API_KEY);
    this.isInitialized = true;
  }

  getInstance() {
    if (!this.isInitialized && typeof window !== 'undefined') {
      this.initialize();
    }

    return this.instance;
  }

  logEvent(event, customProps = {}) {
    this.getInstance().logEvent(event.name, {
      ...customProps,
      ...event.props,
    });
  }

  setUserId(id, userProps) {
    this.getInstance().setUserId(id);
    let identify;

    if (typeof window !== 'undefined') {
      const amplitude = require('amplitude-js');
      identify = new amplitude.Identify();

      Object.keys(userProps).forEach((key) =>
        identify.set(key, userProps[key])
      );
    }

    this.getInstance().identify(identify);
  }

  logOutEvent() {
    this.getInstance().setUserId(null);
    this.getInstance().regenerateDeviceId();
  }
}
