import { ANALYTICS } from '../../config/env.config';
import AmplitudeAnalytics from './amplitude/analytics-amplitude.provider';

class AnalyticsProvider {
  constructor() {
    switch (ANALYTICS.PROVIDER) {
      case 'amplitude':
        this.provider = new AmplitudeAnalytics();
        break;
      default:
        throw new Error(
          `Analytics provider ${ANALYTICS.PROVIDER} not implemented`
        );
    }
  }

  logEvent(event, properties) {
    if (!event) {
      console.warn(`Analytics event does not exist`);
      return;
    }

    this.provider.logEvent(event, properties);
  }

  setUserId(userId, userProps) {
    this.provider.setUserId(userId, userProps);
  }

  logOutEvent() {
    this.provider.logOutEvent();
  }
}

export default new AnalyticsProvider();
