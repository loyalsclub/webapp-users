export default {
  registrationFromStore: {
    name: 'join store club',
    props: {
      category: 'registration',
    },
  },
  registrationGoogle: {
    name: 'inited registration',
    props: {
      category: 'registration',
      type: 'google',
    },
  },
  registrationFacebook: {
    name: 'inited registration',
    props: {
      category: 'registration',
      type: 'facebook',
    },
  },
  registrationLoyals: {
    name: 'inited registration',
    props: {
      category: 'registration',
      type: 'loyals',
    },
  },
  registrationEmailStep: {
    name: 'finished email step',
    props: {
      category: 'registration',
      pending: false,
    },
  },
  registrationEmailStepPending: {
    name: 'finished email step',
    props: {
      category: 'registration',
      pending: true,
    },
  },
  registrationUserDataStep: {
    name: 'finished user step',
    props: {
      category: 'registration',
    },
  },
  registrationCompleted: {
    name: 'finished registration',
    props: {
      category: 'registration',
    },
  },
  registerCoupon: {
    name: 'register coupon',
    props: {},
  },
};
