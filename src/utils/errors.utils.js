/** This function recieves an object that has the following format
var err = {
  graphQLErrors: {
    code: ----
    response: -----
  },
  networkError: [{
    message: STRING
  }]
}
And returns the adequate message for the error.
*/

import Reports from '../providers/Reports';
import translate from './i18n.utils';

const translateError = (fn) => (...args) => {
  const err = fn(...args);

  if (err && err.text) {
    return {
      text: translate(err.text),
    };
  }

  return '';
};

const parseError = translateError((err = {}) => {
  if (err.networkError) {
    if (err.networkError.statusCode === 400) {
      return SERVER_ERROR;
    }

    Reports.captureException(err);

    return NETWORK_ERROR;
  }

  // Handle Server API Errors
  if (err.graphQLErrors && err.graphQLErrors.length > 0) {
    const error = err.graphQLErrors[0];
    const message = API_ERRORS[error.message];
    if (message) return message;

    Reports.captureException(err);

    return GENERIC_ERROR;
  }

  return null;
});

const parseFirebaseError = translateError((err = {}) => {
  const message = err.code;
  return FIREBASE_ERRORS[message] ? FIREBASE_ERRORS[message] : GENERIC_ERROR;
});

const GENERIC_ERROR = {
  text: 'ERRORS.generic',
};

const SERVER_ERROR = {
  text: 'ERRORS.server',
};

const NETWORK_ERROR = {
  text: 'ERRORS.network',
};

const API_ERRORS = {
  'auth/user-is-not-authorized': {
    text: 'ERRORS.apiUserIsNotAuthorized',
  },
  'api/user-not-found': {
    text: 'ERRORS.apiUserNotFound',
  },
  'api/user-is-not-a-customer': {
    text: 'ERRORS.apiUserIsNotACustomer',
  },
  'api/transaction-not-found': {
    text: 'ERRORS.apiTransactionNotFound',
  },
  'api/transaction-redeemed': {
    text: 'ERRORS.apiTransactionRedeemed',
  },
};

const FIREBASE_ERRORS = {
  'auth/user-not-found': {
    text: 'ERRORS.firebaseUserNotFound',
  },
  'auth/wrong-password': {
    text: 'ERRORS.firebaseWrongPassword',
  },
  'auth/invalid-email': {
    text: 'ERRORS.firebaseInvalidEmail',
  },
};

export { parseError, parseFirebaseError, SERVER_ERROR };
