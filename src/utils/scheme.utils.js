const geo = (coords) => {
  const [lat, lng] = coords;
  const url = `https://www.google.com/maps/search/?api=1&query=${lat},${lng}`;

  return url;
};

const phone = (number) => {
  const url = `tel:${number}`;

  return url;
};

const redirectUrls = [
  'complete-profile',
  'login',
  'register',
  'recover-password',
];

const isStoreUrl = (path) => {
  const urlBase = path.split('/');
  return (
    urlBase.length > 1 &&
    urlBase[1].length > 0 &&
    redirectUrls.indexOf(urlBase[1]) === -1
  );
};

const getStoreFromUrl = (pathname) => {
  const urlBase = pathname.split('/');
  if (urlBase.length > 1 && redirectUrls.indexOf(urlBase[1]) > -1) return null;
  return urlBase[1];
};

const getQueryString = (path) =>
  path.indexOf('?') > 0 ? path.substring(path.indexOf('?') + 1) : '';

const getPathWithoutQueryString = (path) =>
  path.indexOf('?') > 0 ? path.substring(0, path.indexOf('?')) : path;

const getSlugPiecesFromPath = (path) =>
  getPathWithoutQueryString(path).split('/').filter(Boolean);

export {
  geo,
  phone,
  getStoreFromUrl,
  isStoreUrl,
  getQueryString,
  getPathWithoutQueryString,
  getSlugPiecesFromPath,
};
