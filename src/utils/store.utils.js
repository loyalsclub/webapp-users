import { SOCIAL_MEDIAS_DOMAIN } from '../config/constants/store.constants';

const addSocialMediaDomain = (socialMedia, path) => {
  const domain = SOCIAL_MEDIAS_DOMAIN[socialMedia];

  if (domain && !path.includes(domain)) {
    return `https://${domain}/${path}`;
  }

  return path;
};

const addCountryCodeToNumber = (number, areaCode) => {
  let phoneNumber = number.split('/').pop().replace(/\s|\+/g, '');

  const hasAreaCode = phoneNumber.substring(0, areaCode.length) === areaCode;

  if (!hasAreaCode) {
    phoneNumber = `${areaCode}${phoneNumber}`;
  }

  return phoneNumber;
};

export { addSocialMediaDomain, addCountryCodeToNumber };
