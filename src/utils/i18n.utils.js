import I18n from 'i18n-js'; // You can import i18n-js as well if you don't want the app to set default locale from the device locale.
import es from '../config/language/es';
import en from '../config/language/en';
import mx from '../config/language/mx';

// If an english translation is not available in en.js, it will look inside hi.js
I18n.fallbacks = true;
I18n.missingBehaviour = 'guess'; // It will convert HOME_noteTitle to 'HOME note title' if the value of HOME_noteTitle doesn't exist in any of the translation files.
I18n.defaultLocale = 'es'; // If the current locale in device is not en or hi
I18n.locale = 'es'; // If we do not want the framework to use the phone's locale by default

const locales = {
  es,
  en,
  mx,
};

I18n.translations = locales;

const translate = I18n.translate.bind(I18n);

export const setLocale = (code) => {
  const codeLowerCased = code.toLowerCase();
  const locale = locales[codeLowerCased] ? codeLowerCased : 'es';
  const hasChanged = locale !== getCurrentLocale();

  if (hasChanged) {
    I18n.locale = locale;
    document.dispatchEvent(new window.CustomEvent('language-changed'));
  }

  const defaultLocale = getDefaultLocale();
  const isDifferentThanDefault = locale !== defaultLocale;

  if (isDifferentThanDefault) {
    return () => setLocale(defaultLocale);
  }
};

export const getCurrentLocale = () => I18n.locale;

export const getDefaultLocale = () => I18n.defaultLocale;

export default translate;
