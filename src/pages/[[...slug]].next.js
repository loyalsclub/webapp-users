/** External dependencies */
import React, { useState, useEffect } from 'react';
import { withApollo } from '@apollo/client/react/hoc';
import { useQuery } from '@apollo/client';
import { useRouter } from 'next/router';
import Head from 'next/head';
import dynamic from 'next/dynamic';

/** Queries */
import { IS_VALID_USER, STORE_METADATA } from '../queries/user.queries';

/** Components */
import CrownLoaderComponent from '../components/Common/CrownLoader.component';

/** Providers */
import { withAuthentication } from '../providers/Authentication';
import { analyticsProvider } from '../providers/analytics';
import { initializeApollo } from '../providers/Apollo';
import { STORE_GET_CATEGORIES_AND_COUNTRIES } from '../queries/store.queries';

// Routers
const AuthHomeRoutes = dynamic(() => import('../routes/AuthHome.routes'));
const UserHomeRoutes = dynamic(() => import('../routes/UserHome.routes'));
const Page500 = dynamic(() => import('../components/Error/500.page'));

const loading = () => (
  <div className="full-centered-container animated fadeIn pt-3 text-center">
    <CrownLoaderComponent />
  </div>
);

const CatchAllRoutes = ({ authObj, client }) => {
  const router = useRouter();

  const [authenticated, setAuthenticated] = useState(false);
  const [isLoading, setLoading] = useState(true);

  const logOut = () => {
    analyticsProvider.logOutEvent();
    authObj.doSignOut();
    client.clearStore();
    router.push('/');
    setAuthenticated(false);
  };

  const setAuth = () => {
    setAuthenticated(true);
  };

  useEffect(() => {
    const unsubscribe = authObj.auth.onAuthStateChanged(async (user) => {
      if (user) {
        try {
          const { data } = await client.query({
            query: IS_VALID_USER,
          });

          setAuthenticated(data && data.isValidUser);
        } catch (error) {
          console.warn('Error IS_VALID_USER', error);
        }
      }

      setLoading(false);
    });

    return function cleanup() {
      unsubscribe();
    };
  }, [authObj.auth, client]);

  const renderBasedOnSlugAndAuthState = (slug = ['/']) => {
    const firstPiece = slug[0];
    const isExact = slug.length === 1 && slug[0] === '/';

    switch (firstPiece) {
      case '500':
      case '400':
        return <Page500 />;
      default:
        return authenticated ? (
          <UserHomeRoutes logOut={logOut} isExact={isExact} />
        ) : (
          <AuthHomeRoutes logOut={logOut} setAuth={setAuth} isExact={isExact} />
        );
    }
  };

  const { data: { storeInfoByAlias: store } = {} } = useQuery(STORE_METADATA, {
    skip: !router.query.slug || !router.query.slug.length,
    variables: { alias: router.query.slug && router.query.slug[0] },
  });

  useQuery(STORE_GET_CATEGORIES_AND_COUNTRIES, {
    skip: !router.query.slug || !router.query.slug.length,
  });

  return (
    <>
      {store && (
        <Head>
          <title>{store.name} - Loyals Club</title>

          <meta name="og:title" content={store.name} key="og:title" />

          <meta
            name="description"
            content={store.description}
            key="description"
          />

          <meta
            name="og:description"
            content={store.description}
            key="og:description"
          />

          <link
            rel="icon"
            type="image/png"
            key="favicon"
            href={store.profileImage}
          />

          <meta property="og:image" content={store.profileImage} />

          <meta name="robots" content="noindex" />
        </Head>
      )}

      {isLoading || typeof window === 'undefined'
        ? loading()
        : renderBasedOnSlugAndAuthState(router.query.slug)}
    </>
  );
};

export async function getServerSideProps(context) {
  const apolloClient = initializeApollo();

  const alias = context.query.slug && context.query.slug[0];

  if (alias) {
    try {
      await apolloClient.query({
        query: STORE_METADATA,
        variables: { alias },
      });
    } catch {
      console.warn(`[PreFetch Store] Not valid alias: ${alias}`);
    }
  }

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
  };
}

export default withAuthentication(withApollo(CatchAllRoutes));
