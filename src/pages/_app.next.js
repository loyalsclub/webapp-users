import React, { useEffect, useRef } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import { ApolloProvider } from '@apollo/client';
import Modal from 'react-modal';
import Authentication, {
  AuthenticationContext,
} from '../providers/Authentication';
import Reports from '../providers/Reports';
import ENV from '../config/env.config';
import '../scss/main.scss';
import { useApollo } from '../providers/Apollo';
import notificationsProvider from '../providers/notifications/notifications.provider';

const OnBrowserLoad = dynamic(import('../components/OnBrowserLoad.component'), {
  ssr: false,
});

Reports.initialize();

Modal.setAppElement('#__next');

export function reportWebVitals(metric) {
  if (ENV.DEBUGGING_METRICS) {
    console.log(metric);
  }
}

const App = ({ Component, pageProps }) => {
  const apolloClient = useApollo(pageProps.initialApolloState);
  const Auth = useRef(new Authentication(apolloClient));

  useEffect(function initNotificationsWorker() {
    window.addEventListener('load', function load() {
      notificationsProvider.initialize();
    });
  }, []);

  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width,minimum-scale=1.0,initial-scale=1.0,maximum-scale=1.0,user-scalable=no,viewport-fit=cover"
        />

        <meta
          name="description"
          content="Vinculate con tus comercios favoritos."
          key="description"
        />

        <meta
          name="og:description"
          content="Vinculate con tus comercios favoritos."
          key="og:description"
        />

        <link
          rel="icon"
          type="image/png"
          href="/icons/favicon-16x16.png"
          sizes="16x16"
          key="favicon"
        />
        <link
          rel="icon"
          type="image/png"
          href="/icons/favicon-32x32.png"
          sizes="32x32"
          key="favicon"
        />
        <link
          rel="icon"
          type="image/png"
          href="/icons/favicon-96x96.png"
          sizes="96x96"
          key="favicon"
        />

        <title>Loyals Club App</title>

        <meta name="og:title" content="Loyals Club App" key="og:title" />
      </Head>

      <ApolloProvider client={apolloClient}>
        <AuthenticationContext.Provider value={Auth.current}>
          <Component {...pageProps} />
        </AuthenticationContext.Provider>
      </ApolloProvider>

      <OnBrowserLoad />
    </>
  );
};

export default App;
