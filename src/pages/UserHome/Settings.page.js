/** External dependencies */
import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/client';

/** Queries */
import { USER_INFO, UPDATE_USER } from '../../queries/user.queries';

/** Utils */
import { parseError } from '../../utils/errors.utils';

/** Components */
import SettingsComponent from '../../components/UserHome/Settings.component';

const SettingsPage = ({ logout, redirectToSettings, action }) => {
  const { error: queryError, loading, data = {} } = useQuery(USER_INFO, {
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  const [reqError, setError] = useState(null);

  const handleRequestError = (requestError) => {
    const err = parseError(requestError);

    if (!err) {
      return;
    }

    setError({ text: err.text });
  };

  const [updateUser] = useMutation(UPDATE_USER, {
    onCompleted: () => redirectToSettings(),
    onError: handleRequestError,
  });

  const { userInfo } = data;

  return (
    <SettingsComponent
      userInfo={userInfo}
      loading={loading}
      logout={logout}
      reqError={reqError}
      partialData={userInfo !== undefined}
      updateUser={updateUser}
      error={parseError(queryError)}
      action={action}
    />
  );
};

export default SettingsPage;
