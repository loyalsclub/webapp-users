/** External dependencies */
import React from 'react';
import { useQuery } from '@apollo/client';

/** Queries */
import { useRouter } from 'next/router';
import { USER_INFO } from '../../queries/user.queries';

/** Utils */
import { parseError } from '../../utils/errors.utils';

/** Components */
import UserProfileComponent from '../../components/UserHome/UserProfile.component';

const UserProfile = () => {
  const router = useRouter();

  const { error: queryError, loading, data = {} } = useQuery(USER_INFO, {
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true,
  });

  const goBack = () => router.back();

  const { userInfo } = data;

  return (
    <UserProfileComponent
      userInfo={userInfo}
      goBack={goBack}
      loading={loading}
      partialData={userInfo !== undefined}
      error={parseError(queryError)}
    />
  );
};

export default UserProfile;
