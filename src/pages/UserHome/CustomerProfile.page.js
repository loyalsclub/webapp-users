/** External dependencies */
import React, { useState, useEffect } from 'react';
import { useQuery, useLazyQuery, useMutation } from '@apollo/client';
import { useRouter } from 'next/router';

/** Providers */
import { analyticsProvider, events } from '../../providers/analytics';

/** Utils */
import { parseError } from '../../utils/errors.utils';

/** Components */
import CustomerProfileComponent from '../../components/UserHome/CustomerProfile.component';

/** Queries */
import { STORE_INFO } from '../../queries/user.queries';
import {
  USER_CUSTOMER_PROFILE_BY_STORE_ID,
  USER_CUSTOMER_REGISTER_COUPON,
} from '../../queries/customer.queries';
import {
  getPathWithoutQueryString,
  getQueryString,
} from '../../utils/scheme.utils';

const CustomerProfile = ({ alias }) => {
  const router = useRouter();
  const queryString = getQueryString(router.asPath);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [pointsLoaded, setPointsLoaded] = useState(0);
  const [pointsError, setPointsError] = useState(0);
  const [storeId, setStoreId] = useState('');

  const [getCustomer, { data = {}, refetch }] = useLazyQuery(
    USER_CUSTOMER_PROFILE_BY_STORE_ID,
    {
      fetchPolicy: 'cache-and-network',
      notifyOnNetworkStatusChange: true,
      onCompleted: () => {
        setLoading(false);
      },
      onError: (queryError) => {
        setLoading(false);
        const err = parseError(queryError);

        if (!err) return;
        setError({
          text: err.text,
        });
      },
    }
  );

  useQuery(STORE_INFO, {
    variables: { alias },
    fetchPolicy: 'cache-and-network',
    onCompleted: (response) => {
      if (!response.storeInfoByAlias) {
        setError({
          text: 'No se encuentra el comercio pedido :(',
        });
        setLoading(false);
        return;
      }

      const { id } = response.storeInfoByAlias;

      setStoreId(id);

      getCustomer({ variables: { storeId: id } });
    },
    notifyOnNetworkStatusChange: true,
  });

  const [registerCoupon] = useMutation(USER_CUSTOMER_REGISTER_COUPON, {
    onCompleted: ({ registerCouponPoints }) => {
      analyticsProvider.logEvent(events.registerCoupon, {
        alias,
        valid: true,
      });
      /** Replace path to avoid user to go back and resubmit */
      refetch().then(() => {
        router.replace({
          pathname: getPathWithoutQueryString(router.asPath),
          search: `points-loaded=${registerCouponPoints.amount}`,
        });
      });
    },
    onError: (registerCouponError) => {
      analyticsProvider.logEvent(events.registerCoupon, {
        alias,
        valid: false,
        error: registerCouponError.graphQLErrors[0].message,
      });

      router.replace({
        pathname: getPathWithoutQueryString(router.asPath),
        search: 'points-error=true',
      });
    },
  });

  const onUserProfile = () => router.push('/p');

  useEffect(
    function parseCouponCode() {
      if (storeId) {
        const urlParams = new URLSearchParams(decodeURIComponent(queryString));
        const code = urlParams.get('cc');
        const pLoaded = urlParams.get('points-loaded');
        const pError = urlParams.get('points-error');

        if (code) {
          registerCoupon({ variables: { storeId, couponCode: code } });
        }

        if (pLoaded && !Number.isNaN(pLoaded)) {
          setPointsLoaded(pLoaded);
        }

        if (pError) {
          setPointsError(pError);
        }
      }
    },
    [queryString, registerCoupon, storeId]
  );

  const {
    userCustomerProfileByStoreId,
    customerProfileGetStoreLevelsWithRewards,
  } = data;

  return (
    <CustomerProfileComponent
      loading={loading}
      error={error}
      pointsLoaded={pointsLoaded}
      pointsError={pointsError}
      onUserProfile={onUserProfile}
      partialData={
        userCustomerProfileByStoreId !== undefined &&
        customerProfileGetStoreLevelsWithRewards !== undefined
      }
      userCustomerProfileByStoreId={userCustomerProfileByStoreId}
      customerProfileGetStoreLevelsWithRewards={
        customerProfileGetStoreLevelsWithRewards
      }
    />
  );
};

export default CustomerProfile;
