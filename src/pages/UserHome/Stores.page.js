/** External dependencies */
import React from 'react';
import { useQuery } from '@apollo/client';

/** Utils */
import { parseError } from '../../utils/errors.utils';

/** Components */
import StoresComponent from '../../components/UserHome/Stores.component';

/** Queries */
import { USER_CUSTOMER_PROFILES } from '../../queries/customer.queries';

const StoresPage = () => {
  const { error: queryError, data = {}, loading } = useQuery(
    USER_CUSTOMER_PROFILES,
    {
      fetchPolicy: 'cache-and-network',
      notifyOnNetworkStatusChange: true,
    }
  );

  const { userCustomerProfiles } = data;

  return (
    <StoresComponent
      loading={loading}
      error={parseError(queryError)}
      partialData={userCustomerProfiles !== undefined}
      userCustomerProfiles={userCustomerProfiles}
    />
  );
};

export default StoresPage;
