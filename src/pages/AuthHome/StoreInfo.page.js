/** External dependencies */
import React, { useState } from 'react';
import { useQuery, useLazyQuery } from '@apollo/client';
import { useRouter } from 'next/router';

/** Utils */
import { parseError } from '../../utils/errors.utils';

/** Components */
import StoreInfoComponent from '../../components/AuthHome/StoreInfo.component';

/** Queries */
import {
  STORE_INFO,
  STORE_LEVELS_AND_REWARDS_BY_ID,
} from '../../queries/user.queries';

/** Providers */
import { events, analyticsProvider } from '../../providers/analytics';
import translate from '../../utils/i18n.utils';
import useUpdateLanguageListener from '../../hooks/useUpdateLanguageListener';

const StoreInfoPage = () => {
  const router = useRouter();
  const alias = router.query.slug[0];

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const handleRequestError = (queryError) => {
    const err = parseError(queryError);

    if (!err) return;
    setError({
      text: err.text,
    });
  };

  const { data: storeInfoData = {} } = useQuery(STORE_INFO, {
    variables: { alias },
    fetchPolicy: 'cache-and-network',
    onCompleted: (response) => {
      if (!response.storeInfoByAlias) {
        setError({
          text: translate('ERRORS.storeNotFound'),
        });

        setLoading(false);
        return;
      }

      const storeId = response.storeInfoByAlias.id;

      getStoreRewards({ variables: { storeId } });
    },
    onError: handleRequestError,
    notifyOnNetworkStatusChange: true,
  });

  const [getStoreRewards, { data = {} }] = useLazyQuery(
    STORE_LEVELS_AND_REWARDS_BY_ID,
    {
      notifyOnNetworkStatusChange: true,
      onCompleted: () => {
        setLoading(false);
      },
      fetchPolicy: 'cache-and-network',
      onError: handleRequestError,
    }
  );

  const onJoin = () => {
    analyticsProvider.logEvent(events.registrationFromStore, { alias });

    router.push({
      pathname: '/',
      search: `redirect-url=${encodeURIComponent(router.asPath)}`,
    });
  };

  const { storeInfoByAlias: Store = {} } = storeInfoData;
  const { storeLevelsAndRewardsById } = data;

  useUpdateLanguageListener(Store && Store.countryCode);

  return (
    <StoreInfoComponent
      loading={loading}
      error={error}
      onJoin={onJoin}
      Store={Store}
      storeLevelsAndRewardsById={storeLevelsAndRewardsById}
    />
  );
};

export default StoreInfoPage;
