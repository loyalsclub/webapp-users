/** External dependencies */
import React, { useEffect } from 'react';

/** Providers */
import { withAuthentication } from '../../providers/Authentication';

/** Components */
import GeneralAuthComponent from '../../components/AuthHome/GeneralAuth.component';

const GeneralAuthPage = ({ storeData, authObj, redirectTo }) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  const loginOAuth = (provider) => () => {
    const auth = authObj;
    const providers = {
      facebook: auth.doSignInWithFacebook,
      google: auth.doSignInWithGoogle,
    };

    if (!providers[provider]) return;

    providers[provider]()
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <GeneralAuthComponent
      storeData={storeData}
      redirectTo={redirectTo}
      loginGoogle={loginOAuth('google')}
      loginFacebook={loginOAuth('facebook')}
    />
  );
};

export default withAuthentication(GeneralAuthPage);
