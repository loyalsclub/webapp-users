/** External dependencies */
import React, { useState, useEffect } from 'react';
import { useLazyQuery, useMutation } from '@apollo/client';

/** Providers */
import { withAuthentication } from '../../providers/Authentication';
import { events, analyticsProvider } from '../../providers/analytics';

/** Components */
import CompleteProfileComponent from '../../components/AuthHome/CompleteProfile.component';

/** Queries */
import {
  FIND_PENDING_USER,
  COMPLETE_USER_SOCIAL_LOGIN,
  IS_VALID_USER,
} from '../../queries/user.queries';

/** Utils */
import { parseError } from '../../utils/errors.utils';
import translate from '../../utils/i18n.utils';

const CompleteProfilePage = (props) => {
  const { authObj, setAuth, logOut } = props;
  const [currentStep, setCurrentStep] = useState(1);
  const [newUser, setNewUser] = useState({});
  const [error, setError] = useState(null);

  useEffect(() => {
    const currentUser = authObj.getCurrentUser();
    if (!currentUser) return;

    const { displayName } = currentUser;
    const firstName = displayName && displayName.split(' ')[0];
    const lastName =
      displayName &&
      displayName.substring(
        firstName.length + 1,
        currentUser.displayName.length
      );

    /** Analyticcs */
    const provider = currentUser.providerData[0];
    if (provider.providerId === 'google.com') {
      analyticsProvider.logEvent(events.registrationGoogle);
    }
    if (provider.providerId === 'facebook.com') {
      analyticsProvider.logEvent(events.registrationFacebook);
    }
    analyticsProvider.logEvent(events.registrationEmailStep);

    const user = {
      email: currentUser.email || '',
      firstName: firstName || '',
      lastName: lastName || '',
    };
    setNewUser(user);
  }, [authObj]);

  const handleRequestError = (queryError) => {
    const err = parseError(queryError);
    if (!err) return;
    setError({
      text: err.text,
    });
  };

  const [findPendingUserReq] = useLazyQuery(FIND_PENDING_USER, {
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      if (data.findPendingUser && !data.findPendingUser.isPending) {
        return setError({
          text: translate('ERRORS.emailAlreadyRegistered'),
        });
      }
      setError(null);
      setNewUser({ ...newUser, ...data.findPendingUser });
      analyticsProvider.logEvent(events.registrationUserDataStep);
      setCurrentStep(2);
    },
    onError: handleRequestError,
  });

  const findPendingUser = ({ email }) => {
    setNewUser({ ...newUser, email });
    findPendingUserReq({
      variables: {
        email,
      },
    });
  };

  const [completeUserSocialLogin] = useMutation(COMPLETE_USER_SOCIAL_LOGIN, {
    update(cache) {
      return cache.writeQuery({
        query: IS_VALID_USER,
        data: { isValidUser: true },
      });
    },
    onCompleted: () => {
      analyticsProvider.logEvent(events.registrationCompleted);
      return setAuth();
    },
    onError: handleRequestError,
  });

  return (
    <CompleteProfileComponent
      currentStep={currentStep}
      error={error}
      newUser={newUser}
      signOut={logOut}
      findPendingUser={findPendingUser}
      completeUserSocialLogin={completeUserSocialLogin}
    />
  );
};

export default withAuthentication(CompleteProfilePage);
