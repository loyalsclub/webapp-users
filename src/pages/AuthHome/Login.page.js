/** External dependencies */
import React, { useState } from 'react';

/** Providers */
import { withAuthentication } from '../../providers/Authentication';

/** Utils */
import { parseFirebaseError } from '../../utils/errors.utils';

/** Components */
import LoginComponent from '../../components/AuthHome/Login.component';

const LoginPage = ({ authObj }) => {
  const [error, setError] = useState(null);

  const loginEmailPassword = async (data, errorCallback) => {
    setError(null);
    const auth = authObj;
    return auth
      .doSignInWithEmailAndPassword(data.email, data.password)
      .catch((authError) => {
        const pError = parseFirebaseError(authError);
        setError(pError);
        errorCallback();
      });
  };

  return (
    <LoginComponent loginEmailPassword={loginEmailPassword} error={error} />
  );
};

export default withAuthentication(LoginPage);
