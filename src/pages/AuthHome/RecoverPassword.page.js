/** External dependencies */
import React from 'react';

/** Providers */
import { withAuthentication } from '../../providers/Authentication';

/** Components */
import RecoverPasswordComponent from '../../components/AuthHome/RecoverPassword.component';

const RecoverPasswordPage = ({ authObj }) => {
  const recoverPassword = (email) => {
    const auth = authObj;
    return auth.recoverPassword(email);
  };
  return <RecoverPasswordComponent recoverPassword={recoverPassword} />;
};

export default withAuthentication(RecoverPasswordPage);
