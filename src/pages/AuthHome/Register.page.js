/** External dependencies */
import React from 'react';

/** Providers */
import { withAuthentication } from '../../providers/Authentication';

/** Components */
import RegisterComponent from '../../components/AuthHome/Register.component';

const RegisterPage = ({ authObj }) => {
  const loginAfterRegistration = (loginToken) => {
    const auth = authObj;
    return auth.doSignInWithCustomToken(loginToken).catch(() => {});
  };

  return <RegisterComponent loginAfterRegistration={loginAfterRegistration} />;
};

export default withAuthentication(RegisterPage);
