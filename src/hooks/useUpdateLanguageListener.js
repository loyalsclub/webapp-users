import { useEffect, useState } from 'react';
import { getDefaultLocale, setLocale } from '../utils/i18n.utils';

const useUpdateLanguageListener = (countryCode = getDefaultLocale()) => {
  const [, setState] = useState();

  useEffect(
    function updateLocale() {
      return setLocale(countryCode);
    },
    [countryCode]
  );

  useEffect(function subscribeToLanguageChange() {
    function updateState() {
      setState((state) => !state);
    }

    document.addEventListener('language-changed', updateState);

    return () => document.removeEventListener('language-changed', updateState);
  }, []);
};

export default useUpdateLanguageListener;
