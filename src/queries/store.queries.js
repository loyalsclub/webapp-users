import { gql } from '@apollo/client';

const STORE_GET_CATEGORIES_AND_COUNTRIES = gql`
  query getCategoriesAndCountries {
    storeCategories {
      name
      displayName
    }
    storeCountries {
      name
      displayName
      conversionRate
      areaCode
    }
  }
`;

const GET_STORE_INFORMATION = gql`
  query getStoreInformation($alias: String!) {
    storeInfoByAlias(alias: $alias) {
      description
      StoreLocations {
        id
        address
        geo {
          type
          coordinates
        }
        email
        phone
        website
      }
      selectedCategory @client {
        name
        displayName
      }
      parsedSocialMedia @client
    }
  }
`;

export { STORE_GET_CATEGORIES_AND_COUNTRIES, GET_STORE_INFORMATION };
