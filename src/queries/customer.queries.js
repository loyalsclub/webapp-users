import { gql } from '@apollo/client';

const USER_CUSTOMER_PROFILES = gql`
  query {
    userCustomerProfiles(status: active) {
      id
      CustomerLevel {
        level
        nextLevelPoints
        activeRewards
        total
      }
      Store {
        id
        profileImage
        name
        alias
        category
      }
    }
  }
`;

const USER_CUSTOMER_PROFILE_BY_STORE_ID = gql`
  query getUserCustomerProfileByStoreId($storeId: ID!) {
    userCustomerProfileByStoreId(storeId: $storeId) {
      id
      CustomerLevel {
        customerId
        currentLevelPoints
        nextLevelPointsDif
        nextLevelPoints
        level
        total
      }
      Store {
        id
        name
        alias
        profileImage
        description
        category
        countryCode
        socialMedia
        StoreLocations {
          id
          address
          geo {
            type
            coordinates
          }
          email
          phone
          website
        }
      }
    }
    customerProfileGetStoreLevelsWithRewards(storeId: $storeId) {
      id
      level
      StoreRewards {
        id
        title
        description
        isConsideredUsed
        UsageFrequency {
          id
          type
          value
        }
      }
    }
  }
`;

const USER_CUSTOMER_REGISTER_COUPON = gql`
  mutation($couponCode: String!, $storeId: ID!) {
    registerCouponPoints(couponCode: $couponCode, storeId: $storeId) {
      id
      amount
    }
  }
`;

export {
  USER_CUSTOMER_PROFILES,
  USER_CUSTOMER_PROFILE_BY_STORE_ID,
  USER_CUSTOMER_REGISTER_COUPON,
};
