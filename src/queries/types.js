export default `
  extend type Store {
    selectedCategory: StoreCategory
    parsedSocialMedia: [SocialMedia]
  }

  type SocialMedia {
    name: String!
    value: String!
  }

  input StoreLocationInput {
    address: String
    geo: GeometryInput
    email: String
    phone: String
  }

  input GeometryInput {
    type: String
    coordinates: [Float]
  }

  enum AllowedStatusCustomerProfile {
    active
    pending
    deleted
  }
`;
