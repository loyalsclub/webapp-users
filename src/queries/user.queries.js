import { gql } from '@apollo/client';

const FIND_PENDING_USER = gql`
  query($email: String!) {
    findPendingUser(email: $email) {
      isPending
      email
      firstName
      lastName
    }
  }
`;

const CREATE_USER = gql`
  mutation(
    $firstName: String!
    $lastName: String!
    $email: Email!
    $birthDate: DateTime!
    $password: String!
  ) {
    createUser(
      firstName: $firstName
      lastName: $lastName
      email: $email
      birthDate: $birthDate
      password: $password
    ) {
      loginToken
    }
  }
`;

const UPDATE_USER = gql`
  mutation($firstName: String, $lastName: String) {
    updateUser(firstName: $firstName, lastName: $lastName) {
      id
      firstName
      lastName
      email
    }
  }
`;

const UPDATE_USER_WEB_PUSH_SUBSCRIPTION = gql`
  mutation updateUserWebPushSubscription($subscription: JSON) {
    updateUserWebPushSubscription(webPushSubscription: $subscription) {
      id
      webPushSubscription
    }
  }
`;

const COMPLETE_PENDING_USER = gql`
  mutation(
    $firstName: String!
    $lastName: String!
    $email: Email!
    $birthDate: DateTime!
    $password: String!
  ) {
    completePendingUser(
      firstName: $firstName
      lastName: $lastName
      email: $email
      birthDate: $birthDate
      password: $password
    ) {
      loginToken
    }
  }
`;

const COMPLETE_USER_SOCIAL_LOGIN = gql`
  mutation(
    $isPending: Boolean
    $firstName: String!
    $lastName: String!
    $email: Email!
    $birthDate: DateTime!
  ) {
    completeUserSocialLogin(
      firstName: $firstName
      lastName: $lastName
      email: $email
      birthDate: $birthDate
      isPending: $isPending
    ) {
      id
    }
  }
`;

const STORE_INFO = gql`
  query getGeneralStoreInfo($alias: String!) {
    storeInfoByAlias(alias: $alias) {
      id
      name
      alias
      profileImage
      description
      category
      countryCode
      socialMedia
      StoreLocations {
        id
        address
        geo {
          type
          coordinates
        }
        email
        phone
        website
      }
    }
  }
`;

const STORE_METADATA = gql`
  query getStoreMetadata($alias: String!) {
    storeInfoByAlias(alias: $alias) {
      id
      name
      alias
      profileImage
      description
    }
  }
`;

const STORE_LEVELS_AND_REWARDS_BY_ID = gql`
  query($storeId: ID!) {
    storeLevelsAndRewardsById(storeId: $storeId) {
      id
      level
      points
      createdAt
      updatedAt
      storeId
      StoreRewards {
        id
        storeId
        storeLevelId
        title
        description
        UsageFrequency {
          id
          type
          value
        }
        StoreRewardUsages {
          id
        }
      }
    }
  }
`;

const USER_INFO = gql`
  query {
    userInfo {
      id
      firstName
      lastName
      alias
      birthDate
      email
      webPushSubscription
    }
  }
`;

const IS_VALID_USER = gql`
  query {
    isValidUser
  }
`;

export {
  COMPLETE_PENDING_USER,
  COMPLETE_USER_SOCIAL_LOGIN,
  CREATE_USER,
  STORE_INFO,
  FIND_PENDING_USER,
  IS_VALID_USER,
  USER_INFO,
  UPDATE_USER,
  STORE_LEVELS_AND_REWARDS_BY_ID,
  STORE_METADATA,
  UPDATE_USER_WEB_PUSH_SUBSCRIPTION,
};
