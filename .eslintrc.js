module.exports = {
  extends: ['airbnb', 'prettier', 'prettier/react'],
  parser: 'babel-eslint',
  plugins: ['react-hooks', 'eslint-plugin-prettier', 'eslint-plugin-react'],
  env: {
    jest: true,
  },
  rules: {
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'no-use-before-define': 'off',
    'react/jsx-filename-extension': 'off',
    'react/prop-types': 'off',
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'comma-dangle': 'off',
    'prettier/prettier': ['error', { singleQuote: true }],
    'global-require': 'warn',
    'import/no-unresolved': 'off',
    'react/jsx-props-no-spreading': 'off',
    'no-plusplus': 'off',
    'consistent-return': 'off',
    'jsx-a11y/label-has-associated-control': [
      'error',
      {
        required: {
          some: ['nesting', 'id'],
        },
      },
    ],
    'jsx-a11y/label-has-for': [
      'error',
      {
        required: {
          some: ['nesting', 'id'],
        },
      },
    ],
    'jsx-a11y/anchor-is-valid': ['error', {
      components: ['Link'],
      specialLink: ['hrefLeft', 'hrefRight'],
      aspects: ['invalidHref', 'preferButton']
    }],
    'prefer-arrow-callback': 'off',
  },
  globals: {
    fetch: false,
    window: true,
    document: true,
    navigator: true,
    localStorage: true,
  },
};
