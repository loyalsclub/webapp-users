/* eslint-env browser, serviceworker, es6 */
/* eslint-disable no-restricted-globals */

function urlB64ToUint8Array(base64String) {
  const padding = '='.repeat((4 - (base64String.length % 4)) % 4);

  const base64 = (base64String + padding).replace(/-/g, '+').replace(/_/g, '/');

  const rawData = window.atob(base64);

  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }

  return outputArray;
}

self.addEventListener('install', () => {
  const urlParams = new URLSearchParams(location.search);
  self.config = Object.fromEntries(urlParams);

  console.log('[Service Worker]', self.config.applicationServerPublicKey);
});

self.addEventListener('push', function push(event) {
  console.log('[Service Worker] Push Received.');
  console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

  const areNotificationsAllowed = Notification.permission === 'granted';

  if (areNotificationsAllowed) {
    const { title, body, icon, alias } = JSON.parse(event.data.text());

    const options = {
      body,
      icon,
      badge: icon,
      data: { alias },
    };

    event.waitUntil(self.registration.showNotification(title, options));
  }
});

self.addEventListener('notificationclick', function notificationclick(event) {
  console.log('[Service Worker] Notification click Received.', event);

  const { alias } = event.notification.data;

  event.notification.close();

  event.waitUntil(clients.openWindow(`${self.location.origin}/${alias}`));
});

self.addEventListener(
  'pushsubscriptionchange',
  function pushsubscriptionchange(event) {
    console.log("[Service Worker]: 'pushsubscriptionchange' event fired.");

    const applicationServerKey = urlB64ToUint8Array(
      self.config.applicationServerPublicKey
    );

    event.waitUntil(
      self.registration.pushManager
        .subscribe({
          userVisibleOnly: true,
          applicationServerKey,
        })
        .then((newSubscription) => {
          // TODO: Send to application server
          console.log('[Service Worker] New subscription: ', newSubscription);
        })
    );
  }
);
