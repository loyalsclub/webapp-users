const withPWA = require('next-pwa');

module.exports = withPWA({
  pwa: {
    dest: 'public',
  },
  images: {
    domains: [process.env.S3_IMAGES_DOMAIN],
  },
  pageExtensions: ['next.js'],
});
